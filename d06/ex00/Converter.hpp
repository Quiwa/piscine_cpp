#ifndef CONVERTER_H
# define CONVERTER_H

#include <iostream>
#include <string>

class Converter{

public:

  Converter(char* str);

	/*
	** CANONICAL FUNCS
	*/
	Converter(Converter const & src);
	~Converter(void);
	Converter & operator=(Converter const & rhs);

private:
  char _cVal;
  int _iVal;
  float _fVal;
  double _dVal;


  /*
  ** BOOLS
  */
  bool isParamChar(std::string msg) const;
  bool isParamInt(std::string msg) const;
  bool isParamFloat(std::string msg) const;
  bool isParamDouble(std::string msg) const;

  bool isFloatSpecialVal(std::string msg) const;
  bool isDoubleSpecialVal(std::string msg) const;

  bool hasSignificantNb(std::string) const;

  /*
  ** CONVERTERS
  */
  void toChar(std::string);
  void toInt (std::string);
  void toFloat (std::string);
  void toDouble (std::string);

  /*
  ** DISPLAYERS
  */
  void displayInt(bool isSpec = false) const;
  void displayChar(bool isImpossible = false) const;
  void displayFloat(bool isSpec = false) const;
  void displayDouble(bool isSpec = false) const;

  /*
  ** ACCESSORS
  */
  char getCVal(void) const;
  int getIVal(void) const;
  float getFVal(void) const;
  double getDVal(void) const;


	Converter(void);
};

#endif

#ifndef C_H
# define C_H

#include <iostream>
#include <string>
#include "Base.hpp"

class C : public Base{

public:

	/*
	** CANONICAL FUNCS
	*/
	C(void);
	C(C const & src);
	~C(void);
	C & operator=(C const & rhs);

private:

};

#endif

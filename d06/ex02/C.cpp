#include "C.hpp"
#include <iostream>
#include <string>


/*
** CANONICAL FUNCS
*/
C::C(void){
	return;
}

C::C(C const & src){
	*this = src;
	return;
}

C &	C::operator=(C const & rhs){
  (void)rhs;
	return *this;
}

C::~C(void){
	return;
}

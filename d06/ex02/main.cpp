#include "Base.hpp"
#include "A.hpp"
#include "B.hpp"
#include "C.hpp"

void identify_from_pointer(Base * p);
void identify_from_reference( Base & p);

int main(){
  A baseA;
  B baseB;
  C baseC;

  std::cout << "Sending A address" << std::endl;
  identify_from_pointer(&baseA);
  std::cout << "Sending B address" << std::endl;
  identify_from_pointer(&baseB);
  std::cout << "Sending C address" << std::endl;
  identify_from_pointer(&baseC);
  std::cout << "Sending B address" << std::endl;
  identify_from_pointer(&baseB);
  std::cout << std::endl;

  std::cout << "Sending A ref" << std::endl;
  identify_from_reference(baseA);
  std::cout << "Sending B ref" << std::endl;
  identify_from_reference(baseB);
  std::cout << "Sending C ref" << std::endl;
  identify_from_reference(baseC);
  std::cout << "Sending A ref" << std::endl;
  identify_from_reference(baseA);
  return (0);
}

void identify_from_pointer(Base * p){
  try{
    A * typeA = dynamic_cast<A *>(p);
    if (typeA != NULL)
      std::cout << "A" << std::endl;
  }
  catch (std::exception e){
    std::cout << e.what();
  }
  try{
    B * typeB = dynamic_cast<B *>(p);
    if (typeB != NULL)
      std::cout << "B" << std::endl;
  }
  catch (std::exception e){
    std::cout << e.what();
  }
  try{
    C * typeC = dynamic_cast<C *>(p);
    if (typeC != NULL)
      std::cout << "C" << std::endl;
  }
  catch (std::exception e){
    std::cout << e.what();
  }
}

void identify_from_reference( Base & p){
  identify_from_pointer(&p);
}

#ifndef B_H
# define B_H

#include <iostream>
#include <string>
#include "Base.hpp"

class B : public Base{

public:

	/*
	** CANONICAL FUNCS
	*/
	B(void);
	B(B const & src);
	~B(void);
	B & operator=(B const & rhs);

private:

};

#endif

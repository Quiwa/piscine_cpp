#include <iostream>
#include "Test.hpp"

template <typename A, typename I, typename F>
void iter(A *array, I const size, F & func){
  I i = -1;
  while (++i < size){
    func(array[i]);
  }
}
template <typename T>
void displayArray(T *array){
  for (size_t i = 0; i < 4; i++) {
    std::cout << "idx : " << i << " -> "  << array[i] << std::endl;
  }
  std::cout << std::endl;
}

template< typename T >
void print( T const & x ) { std::cout << x << std::endl; return; }

int main() {
  int testA[] = { 0, 1, 2, 3, 4 };
  Test testB[5];
  char testC[] = {'1', '2', '3', '7'};

  iter<int, int, void(int const &)>( testA, 5, print );
  iter<Test, int, void (Test const &)>( testB, 5, print );
  iter<char, int, void(char const &)>(testC, 4, print);

  return 0;
}

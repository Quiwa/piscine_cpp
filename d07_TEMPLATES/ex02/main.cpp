#include <iostream>
#include "Array.hpp"
#include "Test.hpp"

int main(){
  Array<int> arrayInt(2);
  Array<int> arrayIntCpyInst(arrayInt);
  Array<int> arrayIntCpy;
  arrayIntCpy = arrayInt;

  arrayInt[0] = 1;
  arrayInt[1] = 2;
  std::cout << "array int         "<< arrayInt << std::endl;
  std::cout << "array int cpy     "<< arrayIntCpy << std::endl;
  std::cout << "array int cpyInst "<< arrayIntCpyInst << std::endl;

  Array<char> arrayChar(2);
  Array<char> arrayCharEmpty(0);
  std::cout << "empty array char size 2 " << arrayChar << std::endl;
  std::cout <<  "empty array char size 0 " << arrayCharEmpty << std::endl;

  try{
    std::cout <<  "accessing out of range val :"<< std::endl;
    arrayInt[3] = 3;
  }
  catch (std::out_of_range e){
    std::cout << "Out of range exception" << std::endl;
  }

  Array<Test> arrayTest(3);
  Array<Test> arrayTestEmpty(0);
  arrayTest[0] = 1;
  arrayTest[1] = 2;
  arrayTest[2] = 2;
  std::cout << "array test size 3 " << arrayTest << std::endl;
  std::cout <<  "array test size 0 " << arrayTestEmpty << std::endl;
  return (0);
}

#include <iostream>
#include "Contact.hpp"
#include "Displayer.hpp"

int main()
{
	std::string request;
	while (1)
	{
		if (std::cin.eof())
			return (1);
		if (request.compare("SEARCH") == 0)
		{
			std::getline(std::cin, request);
			continue ;
		}
		std::cout << std::endl << std::endl << std::endl << std::endl;
		std::cout << " << MAIN MENU >> " << std::endl;
		std::cout << "What would you like to do ? ADD / SEARCH / EXIT " << std::endl;
		std::getline(std::cin, request);
		if (request.compare("ADD") == 0)
			Contact::addContact();
		else if (request.compare("SEARCH") == 0)
				Contact::searchContact();
		else if (request.compare("EXIT") == 0)
			return (1);
	}
	return (0);
}

#ifndef PRESIDENTIALPARDONFORM_H
# define PRESIDENTIALPARDONFORM_H

#include <iostream>
#include <string>
#include "Form.hpp"

class PresidentialPardonForm : public Form{

public:

	PresidentialPardonForm(std::string path);

	/*
	** CANONICAL FUNCS
	*/
	PresidentialPardonForm(PresidentialPardonForm const & src);
	virtual ~PresidentialPardonForm(void);
	PresidentialPardonForm & operator=(PresidentialPardonForm const & rhs);

private:
  std::string _path;

	PresidentialPardonForm(void);
	virtual void action(void) const;

};

#endif

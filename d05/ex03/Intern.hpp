#ifndef INTERN_H
# define INTERN_H

#include <iostream>
#include <string>
#include "Form.hpp"

class Intern{

public:
  typedef struct FormTypes{
    std::string tagName;
    Form *(*make)(std::string);
  } t_formType;

	class UnknownForm : public std::exception{
		public:
			virtual const char * what() const throw();
	};


  Form * makeForm(std::string const & formType, std::string const & formTarget) const;

	/*
	** CANONICAL FUNCS
	*/
	Intern(void);
	Intern(Intern const & src);
	~Intern(void);
	Intern & operator=(Intern const & rhs);

private:
  t_formType _formTypes[3];
  
  static Form * makePresidential(std::string name);
  static Form * makeRobot(std::string name);
  static Form * makeShrubbery(std::string name);

};

#endif

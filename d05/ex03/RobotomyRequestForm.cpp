#include "Form.hpp"
#include "RobotomyRequestForm.hpp"
#include <iostream>
#include <string>
#include <time.h>


RobotomyRequestForm::RobotomyRequestForm(std::string path) :
Form("Robotomy form", 72, 45),
_path(path)
{
}

void RobotomyRequestForm::action(void) const{
  if (time(NULL) % 2 == 0)
    std::cout << "* Makes some drilling noises * <" << _path << "> has been robotomized successfully" << std::endl;
  else
    std::cout << "* no driling noises * It's a failure" << std::endl;
}

/*
** CANONICAL FUNCS
*/
RobotomyRequestForm::RobotomyRequestForm(void) :
Form("Robotomy form", 72, 45)
{
}

RobotomyRequestForm::RobotomyRequestForm(RobotomyRequestForm const & src):
Form::Form(src)
{
	*this = src;
	return;
}

RobotomyRequestForm &	RobotomyRequestForm::operator=(RobotomyRequestForm const & rhs){
  Form::operator=(rhs);
  this->_path = rhs._path;
	return *this;
}

RobotomyRequestForm::~RobotomyRequestForm(void){
	return;
}

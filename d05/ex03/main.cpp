#include "ShrubberyCreationForm.hpp"
#include "RobotomyRequestForm.hpp"
#include "PresidentialPardonForm.hpp"
#include "Bureaucrat.hpp"
#include "Form.hpp"
#include "Intern.hpp"

void tryFormCreation(std::string form, std::string target);

int main(void){
  tryFormCreation("robot", "Bender");
  tryFormCreation("presidential", "Obama");
  tryFormCreation("shrubbery", "LKjhlkj");

  std::cout << std::endl;

  tryFormCreation("shrubberyj", "LKjhlkj");
  tryFormCreation("kjkj", "LKjhlkj");
  return (0);
}

void tryFormCreation(std::string form, std::string target){
  Intern coucou;

  try{
    Form * test = coucou.makeForm(form, target);
    std::cout << form << " successfully created" << std::endl;
    delete(test);
  }
  catch(Intern::UnknownForm e) {std::cout << e.what() << std::endl;}
}

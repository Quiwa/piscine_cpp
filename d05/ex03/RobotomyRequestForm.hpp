#ifndef ROBOTOMYREQUESTFORM_H
# define ROBOTOMYREQUESTFORM_H

#include <iostream>
#include <string>
#include "Form.hpp"

class RobotomyRequestForm : public Form{

public:

	RobotomyRequestForm(std::string path);

	/*
	** CANONICAL FUNCS
	*/
	RobotomyRequestForm(RobotomyRequestForm const & src);
	virtual ~RobotomyRequestForm(void);
	RobotomyRequestForm & operator=(RobotomyRequestForm const & rhs);

private:
  std::string _path;

	RobotomyRequestForm(void);
	virtual void action(void) const;

};

#endif

#ifndef BUREAUCRAT_H
# define BUREAUCRAT_H

#include "Form.hpp"
#include <iostream>
#include <string>

class Bureaucrat{

public:
	class GradeTooLowException : public std::exception{
		public:
			virtual const char * what() const throw();
	};

	class GradeTooHighException : public std::exception{
		public:
			virtual const char * what() const throw();
	};

	void executeForm(Form const & form) const;
	void signForm(Form & form);

  /*
  ** ACCESSORS
  */
	void incrementGrade(void);
	void decrementGrade(void);
	unsigned int getGrade(void) const;
	std::string getName(void) const;

	/*
	** Constructor
	*/
	Bureaucrat(std::string name, unsigned int grade);

	/*
	** CANONICAL FUNCS
	*/
	Bureaucrat(Bureaucrat const & src);
	~Bureaucrat(void);
	Bureaucrat & operator=(Bureaucrat const & rhs);

private:
	const std::string _name;
	unsigned int _grade;

	Bureaucrat(void);
	void setGrade(unsigned int grade);

};

std::ostream& operator<<(std::ostream& os, const Bureaucrat & dt);

#endif

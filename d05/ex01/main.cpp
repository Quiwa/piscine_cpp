#include "Bureaucrat.hpp"
#include "Form.hpp"

Bureaucrat * tryBureaucratCreation(std::string name, unsigned int grade);
Form * tryFormCreation(std::string name, unsigned int toSignGrade, unsigned int toExecGrade);

int main(void){

  Bureaucrat * Bob = tryBureaucratCreation("Bob", 75);
  Bureaucrat BobCpy = Bureaucrat(*Bob);

  tryFormCreation("Form too high", 0, 0);
  tryFormCreation("Form too low", 151, 151);
  Form * formC = tryFormCreation("Form that can sign", 75, 75);
  Form * formD = tryFormCreation("Form that can't sign", 74, 74);
  std::cout << *Bob;
  std::cout << BobCpy;
  std::cout << *formC;
  std::cout << *formD;
  delete(Bob);

  std::cout << std::endl;

  BobCpy.signForm(*formC);
  try{BobCpy.signForm(*formD);}
  catch(Form::GradeTooLowException e){std::cout << e.what() << std::endl;}

  std::cout << std::endl;

  std::cout << *formC;
  std::cout << *formD;
  delete(formC);
  delete(formD);
  return 0;
}

Form * tryFormCreation(std::string name, unsigned int toSignGrade, unsigned int toExecGrade){

  Form * form = NULL;

  try{
    form = new Form(name, toSignGrade, toExecGrade);
    return (form);
  }
  catch (Form::GradeTooLowException e){
    std::cout << name << " " << e.what() << std::endl;
    delete(form);
  }
  catch (Form::GradeTooHighException e){
    std::cout << name << " " << e.what() << std::endl;
    delete(form);
  }
  return (form);
}

Bureaucrat * tryBureaucratCreation(std::string name, unsigned int grade){

  Bureaucrat * bureaucrat = NULL;

  try{
    bureaucrat = new Bureaucrat(name, grade);
    return (bureaucrat);
  }
  catch (Bureaucrat::GradeTooLowException e){
    std::cout << name << " " << e.what() << std::endl;
  }
  catch (Bureaucrat::GradeTooHighException e){
    std::cout << name << " " << e.what() << std::endl;
  }
  return (bureaucrat);
}

#ifndef FORM_H
# define FORM_H

#include <iostream>
#include <string>

class Bureaucrat;

class Form{

public:

	class GradeTooLowException : public std::exception{
		public:
			virtual const char * what() const throw();
	};

	class GradeTooHighException : public std::exception{
		public:
			virtual const char * what() const throw();
	};

  void beSigned(Bureaucrat *bureaucrat);

  /*
  ** ACCESSORS
  */
	void setGrade(unsigned int grade);
	unsigned int getToSignGrade(void) const;
	unsigned int getToExecuteGrade(void) const;
	const std::string & getName(void) const;
	bool getIsSigned(void) const;

  Form(std::string , unsigned int , unsigned int);

	/*
	** CANONICAL FUNCS
	*/
	Form(void);
	Form(Form const & src);
	~Form(void);
	Form & operator=(Form const & rhs);

private:
  const std::string _name;
  const unsigned int _toSignGrade;
  const unsigned int _toExecuteGrade;
  bool _isSigned;

};

std::ostream& operator<<(std::ostream& os, const Form & rhs);

#endif

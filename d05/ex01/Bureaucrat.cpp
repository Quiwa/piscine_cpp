
#include "Bureaucrat.hpp"
#include <iostream>
#include <string>

const char * Bureaucrat::GradeTooLowException::what() const throw(){
	return ("Grade is too low.");
}

const char * Bureaucrat::GradeTooHighException::what() const throw(){
	return ("Grade is too high.");
}

void Bureaucrat::signForm(Form & form){
  if (form.getIsSigned())
		std::cout << "<" << this->getName() << "> cannot sign <" << form.getName() << "> because it's already signed" << std::endl;
	else
		try{
			form.beSigned(this);
			std::cout << "<" << this->getName() << "> signed <" << form.getName() << "> " << std::endl;
		}
		catch (Form::GradeTooLowException e){
			std::cerr << e.what() << std::endl;
		}
}

void Bureaucrat::incrementGrade(void){
	setGrade(getGrade() - 1);
}

void Bureaucrat::decrementGrade(void){
	setGrade(getGrade() + 1);
}

/*
** ACCESSORS
*/
void Bureaucrat::setGrade(unsigned int grade){
	if (grade == 0)
		throw Bureaucrat::GradeTooHighException();
	else if (grade > 150)
		throw Bureaucrat::GradeTooLowException();
	_grade = grade;
}

unsigned int Bureaucrat::getGrade(void) const{
	return (_grade);
}

std::string Bureaucrat::getName(void) const{
	return (_name);
}

Bureaucrat::Bureaucrat(std::string name, unsigned int grade)
:
_name(name)
{
	setGrade(grade);
}

/*
** CANONICAL FUNCS
*/
Bureaucrat::Bureaucrat(void){
	return;
}

Bureaucrat::Bureaucrat(Bureaucrat const & src):
_name(src._name){
	*this = src;
	return;
}

Bureaucrat &	Bureaucrat::operator=(Bureaucrat const & rhs){
	this->setGrade(rhs._grade);
	return *this;
}

Bureaucrat::~Bureaucrat(void){
	return;
}

std::ostream& operator<<(std::ostream& os, const Bureaucrat & rhs){
	os << "<"<< rhs.getName() << ">, bureaucrat grade <" << rhs.getGrade() << ">" <<std::endl;
	return (os);
}

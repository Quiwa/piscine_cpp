#include "Bureaucrat.hpp"

Bureaucrat * tryBureaucratCreation(std::string name, unsigned int grade);
void displayAndDelete(Bureaucrat *bureaucrat);
void display(Bureaucrat *bureaucrat);

int main(void){

  Bureaucrat * Bob = tryBureaucratCreation("Bob", 40);
  Bureaucrat * James = tryBureaucratCreation("James", 150);
  Bureaucrat * HighJames = tryBureaucratCreation("HighJames", 1);
  Bureaucrat * HighJamesCpy = new Bureaucrat(*HighJames);
  Bureaucrat * Jo = tryBureaucratCreation("Jo", 151);
  Bureaucrat * Chantal = tryBureaucratCreation("Chantal", 0);

  display(James);
  try{
    James->decrementGrade();
  }
  catch (Bureaucrat::GradeTooLowException e){
    std::cout << James->getName() << " "<< e.what() << std::endl;
  }
  display(HighJames);
  HighJames->decrementGrade();
  try{
    HighJames->incrementGrade();
    HighJames->incrementGrade();
  }
  catch (Bureaucrat::GradeTooHighException e){
    std::cout << HighJames->getName() << " "<< e.what() << std::endl;
  }
  displayAndDelete(Bob);
  displayAndDelete(James);
  displayAndDelete(HighJames);
  displayAndDelete(Jo);
  displayAndDelete(Chantal);
  displayAndDelete(HighJamesCpy);
  return 0;
}

Bureaucrat * tryBureaucratCreation(std::string name, unsigned int grade){

  Bureaucrat * bureaucrat = NULL;

  try{ bureaucrat = new Bureaucrat(name, grade); }
  catch (Bureaucrat::GradeTooLowException e){ std::cout << name << " " << e.what() << std::endl;}
  catch (Bureaucrat::GradeTooHighException e){std::cout << name << " " << e.what() << std::endl;}
  return (bureaucrat);
}

void displayAndDelete(Bureaucrat *bureaucrat)
{
  if (bureaucrat == NULL)
    return ;
  std::cout << *bureaucrat;
  delete(bureaucrat);
}

void display(Bureaucrat *bureaucrat)
{
  if (bureaucrat == NULL)
    return ;
  std::cout << *bureaucrat;
}

#include "span.hpp"

void outputTests();
void thousandsTests();

int main (void){
  outputTests();
  thousandsTests();
  return (0);
}

void outputTests(){
  span spanThree(3);
  span spanFour(4);

  try{ std::cout << "empty span Three longest span :" << spanThree.longestSpan() << std::endl;}
  catch (...) {std::cout << "span is empty" << std::endl;}
  try {std::cout << "empty span Three shortest span :" << spanThree.shortestSpan() << std::endl;}
  catch (...) {std::cout << "span is empty" << std::endl;}
  try{spanThree.addNumber(98789);}
  catch(std::exception e){std::cout << "Trying to add to much nb" << std::endl;}
  try{spanThree.addNumber(98789);}
  catch(std::exception e){std::cout << "Trying to add to much nb" << std::endl;}
  try{spanThree.addNumber(98789);}
  catch(std::exception e){std::cout << "Trying to add to much nb" << std::endl;}
  try{spanThree.addNumber(98789);}
  catch(std::exception e){std::cout << "Trying to add to much nb" << std::endl;}
  std::cout << "all similar longest span :" << spanThree.longestSpan() << std::endl;
  std::cout << "all similar shortest span :" << spanThree.shortestSpan() << std::endl;

  spanThree = spanFour;
  try{spanThree.addNumber(1);}
  catch(std::exception e){std::cout << "Trying to add to much nb" << std::endl;}
  try{spanThree.addNumber(4);}
  catch(std::exception e){std::cout << "Trying to add to much nb" << std::endl;}
  try{spanThree.addNumber(0);}
  catch(std::exception e){std::cout << "Trying to add to much nb" << std::endl;}
  try{spanThree.addNumber(0);}
  catch(std::exception e){std::cout << "Trying to add to much nb" << std::endl;}
  std::cout << "spanThree longest span :" << spanThree.longestSpan() << std::endl;
  std::cout << "spanThree shortest span :" << spanThree.shortestSpan() << std::endl;

  try{spanFour.addNumber(-4);}
  catch(std::exception e){std::cout << "Trying to add to much nb" << std::endl;}
  try{spanFour.addNumber(4);}
  catch(std::exception e){std::cout << "Trying to add to much nb" << std::endl;}
  try{spanFour.addNumber(0);}
  catch(std::exception e){std::cout << "Trying to add to much nb" << std::endl;}
  try{spanFour.addNumber(1);}
  catch(std::exception e){std::cout << "Trying to add to much nb" << std::endl;}
  std::cout << "spanFour longest span :" << spanFour.longestSpan() << std::endl;
  std::cout << "spanFour shortest span :" << spanFour.shortestSpan() << std::endl;

  span *test = new span(3);
  span *cpy = new span(3);
  test->addNumber(0);
  test->addNumber(1);
  *cpy = *test;
  delete(test);
  delete(cpy);
}

void thousandsTests(){
  span spanTenThousand(10000);

  for (size_t i = 0; i < 10000; i++) {
    spanTenThousand.addNumber(i);
  }
  std::cout << "longest a 10 000 span -> " << spanTenThousand.longestSpan() <<std::endl;
  std::cout << "shortest 10 000 span -> " << spanTenThousand.shortestSpan() <<std::endl;
}

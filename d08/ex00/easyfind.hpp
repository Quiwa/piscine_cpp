#ifndef EASYFIND_H
# define EASYFIND_H

#include <algorithm>

template <typename T>
typename T::iterator easyfind(T  & container, int to_find){
  return (find(container.begin(), container.end(), to_find));
}

#endif

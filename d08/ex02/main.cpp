#include "Mutantstack.hpp"
#include <iostream>
#include <list>
#include <string>

void subjectTests();
template <typename T>void displayStack(MutantStack<T> mutantstack);
void testStack(MutantStack<int> mutantstack, int content);
void testStack(MutantStack<std::string> mutantstack, std::string content);

int main(void){
  MutantStack<int> mutantstack;
  MutantStack<int> *mutantstackTmp = new MutantStack<int>();
  MutantStack<int> *mutantstackCpy = new MutantStack<int>();
  MutantStack<std::string> mutantstackStr;

  std::cout << "SUBJECT TESTS :" << std::endl;
  subjectTests();
  std::cout << "STACK OF INTS :" << std::endl;
  testStack(mutantstack, 2);
  std::cout << "STACK OF STR :" << std::endl;
  testStack(mutantstackStr, "coucou");
  std::cout << "STACK OF CPY:" << std::endl;
  mutantstackTmp->push(0);
  mutantstackTmp->push(1);
  mutantstackTmp->push(2);
  *mutantstackCpy = *mutantstackTmp;
  delete(mutantstackTmp);
  displayStack(*mutantstackCpy);
  delete(mutantstackCpy);
}

void subjectTests()
{
  MutantStack<int> mstack;
  mstack.push(5);
  mstack.push(17);
  std::cout << mstack.top() << std::endl;
  mstack.pop();
  std::cout << mstack.size() << std::endl;
  mstack.push(3);
  mstack.push(5);
  mstack.push(737);
  mstack.push(0);
  MutantStack<int>::iterator it = mstack.begin();
  MutantStack<int>::iterator ite = mstack.end();
  ++it;
  --it;
  while (it != ite)
  {
    std::cout << *it << std::endl;
    ++it;
  }
  std::stack<int> s(mstack);
}

void testStack(MutantStack<int> mutantstack, int content){
  try {std::cout << mutantstack.top() << std::endl;}
  catch (std::out_of_range) {std::cout << "out of range " << std::endl;}
  displayStack(mutantstack);
  mutantstack.push(content);
  displayStack(mutantstack);
  mutantstack.top() = content + 2;
  displayStack(mutantstack);
  mutantstack.pop();
  displayStack(mutantstack);
  mutantstack.pop();
  displayStack(mutantstack);
}

void testStack(MutantStack<std::string> mutantstack, std::string content){
  try {std::cout << mutantstack.top() << std::endl;}
  catch (std::out_of_range) {std::cout << "out of range " << std::endl;}
  displayStack(mutantstack);
  mutantstack.push(content);
  displayStack(mutantstack);
  mutantstack.top() = content + "Improved";
  displayStack(mutantstack);
  mutantstack.pop();
  displayStack(mutantstack);
  mutantstack.pop();
  displayStack(mutantstack);
}

template <typename T>
void displayStack(MutantStack<T> mutantstack){
  std::cout << "<- Stack of size content ->" << std::endl;
  std::cout << "<- Stack of size " << mutantstack.size() << " content ->" << std::endl;
  for (typename MutantStack<T>::iterator it = mutantstack.begin(); it != mutantstack.end(); ++it)
    std::cout << "-> " << *it << std::endl;
  std::cout << ">--- ---<" << std::endl;
}

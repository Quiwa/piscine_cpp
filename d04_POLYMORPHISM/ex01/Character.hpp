#ifndef CHARACTER_H
# define CHARACTER_H

#include <iostream>
#include <string>
#include "AWeapon.hpp"
#include "Enemy.hpp"

class Character{

  public:
    Character(std::string const & name);
    void recoverAP();
    void equip(AWeapon*);
    void attack(Enemy*);

    /*
    ** CANONICAL FUNC
    */
  	Character(Character const & src);
  	~Character(void);
  	Character & operator=(Character const & rhs);
   /*
   ** ACCESSORS
   */
    std::string const & getName() const;
    void setAP(int AP);
    int getAP() const;
    AWeapon * getWeaponCpy() const;
    AWeapon * getWeapon() const;


  private:
    std::string _name;
    int _AP;
    AWeapon *_weapon;

  	Character(void);

};

std::ostream & operator<<(std::ostream & os, Character & character);

#endif

#ifndef PLASMARIFLE_H
# define PLASMARIFLE_H

#include <iostream>
#include <string>
#include "AWeapon.hpp"

class PlasmaRifle : public AWeapon{

public:
  virtual void attack(void) const;

	PlasmaRifle(void);
	PlasmaRifle(PlasmaRifle const & src);
	virtual ~PlasmaRifle(void);
	PlasmaRifle & operator=(PlasmaRifle const & rhs);

private:

};

#endif

#include "SuperMutant.hpp"
#include "Enemy.hpp"
#include <iostream>
#include <string>

void SuperMutant::takeDamage(int damage){
  Enemy::takeDamage((damage >= 3) ? damage - 3 : 0);
}

/*
** CANONICAL FUNCTIONS
*/
SuperMutant::SuperMutant(void){
  this->setHP(170);
  this->setType("Super Mutant");
  std::cout << "Gaaah. Me want smash heads!" << std::endl;
	return;
}

SuperMutant::SuperMutant(SuperMutant const & src){
  std::cout << "Gaaah. Me want smash heads!" << std::endl;
	*this = src;
}

SuperMutant &	SuperMutant::operator=(SuperMutant const & rhs){
  Enemy::operator=(rhs);
	return (*this);
}

SuperMutant::~SuperMutant(void){
  std::cout << "Aaargh..." << std::endl;
	return;
}

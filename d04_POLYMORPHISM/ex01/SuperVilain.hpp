#ifndef SUPERVILAIN_H
# define SUPERVILAIN_H

#include <iostream>
#include <string>
#include "Enemy.hpp"

class SuperVilain : public Enemy{

public:
  virtual void takeDamage(int damage);

	/*
	** CANONICAL FUNCS
	*/
	SuperVilain(void);
	SuperVilain(SuperVilain const & src);
	virtual ~SuperVilain(void);
	SuperVilain & operator=(SuperVilain const & rhs);

private:

};

#endif

#ifndef RADSCORPION_H
# define RADSCORPION_H

#include <iostream>
#include <string>
#include "Enemy.hpp"

class RadScorpion : public Enemy{

public:
  virtual void takeDamage(int damage);

  /*
  ** CANONICAL FUNC
  */
	RadScorpion(void);
	RadScorpion(RadScorpion const & src);
	virtual ~RadScorpion(void);
	RadScorpion & operator=(RadScorpion const & rhs);

private:
};

#endif

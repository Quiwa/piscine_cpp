#ifndef TACTICALMARINE_H
# define TACTICALMARINE_H

#include "ISpaceMarine.hpp"

class TacticalMarine : public ISpaceMarine{
  public:
    virtual TacticalMarine* clone() const;
    virtual void battleCry() const;
    virtual void rangedAttack() const;
    virtual void meleeAttack() const;

    /*
    ** CANONICAL FUNCS
    */
    TacticalMarine(void);
    TacticalMarine(TacticalMarine const & src);
    virtual ~TacticalMarine(void);
  	TacticalMarine & operator=(TacticalMarine const & rhs);

};

#endif

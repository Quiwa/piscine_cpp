#include "TacticalMarine.hpp"
#include "ISpaceMarine.hpp"
#include <iostream>

TacticalMarine* TacticalMarine::clone() const{
  TacticalMarine *cpy = new TacticalMarine();
  *cpy = *this;
  return (cpy);
}

void TacticalMarine::battleCry() const{
  std::cout << "For the holy PLOT!" << std::endl;
}

void TacticalMarine::rangedAttack() const{
  std::cout << "* attacks with a bolter *" << std::endl;
}

void TacticalMarine::meleeAttack() const{
  std::cout << "* attacks with a chainsword *" << std::endl;
}

/*
** CANONICAL FUNCS
*/
TacticalMarine::TacticalMarine(void){
  std::cout << "Tactical Marine ready for battle!" << std::endl;
}

TacticalMarine::TacticalMarine(TacticalMarine const & src){
  std::cout << "Tactical Marine ready for battle!" << std::endl;
  *this = src;
}

TacticalMarine::~TacticalMarine(void){
  std::cout << "Aaargh..." << std::endl;
}

TacticalMarine & TacticalMarine::operator=(TacticalMarine const & ){
  return (*this);
}

#ifndef SQUAD_H
# define SQUAD_H

#include "ISquad.hpp"
#include "ISpaceMarine.hpp"
#include <iostream>

class Squad : public ISquad{
  public:

    struct List {
      /*
      ** CONSTRUCTORS
      */
      List(void){
        next = NULL;
        spaceMarine = NULL;
      }

      List(ISpaceMarine * p_spaceMarine){
        next = NULL;
        spaceMarine = p_spaceMarine;
      }

      /*
      ** DESTRUCTORS
      */
      ~List(void){
        if (spaceMarine != NULL)
          delete (spaceMarine);
        spaceMarine = NULL;
        if (next != NULL)
          delete(next);
        next = NULL;
      }

      /*
      ** ATTRIBUTES
      */
      ISpaceMarine * spaceMarine;
      List * next;
    };

    bool isUnitRegistered(ISpaceMarine *unit) const;
    virtual int push(ISpaceMarine*);

    /*
    ** ACCESSORS
    */
    virtual int getCount() const;
    virtual ISpaceMarine* getUnit(int) const;

    /*
    ** CANONICAL FUNCS
    */
    Squad(void);
    Squad(Squad const & src);
    ~Squad(void);
  	Squad & operator=(Squad const & rhs);

  private:
    List * _units;

};


#endif

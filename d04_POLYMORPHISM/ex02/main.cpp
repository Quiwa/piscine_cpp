#include "Squad.hpp"
#include "ISpaceMarine.hpp"
#include "TacticalMarine.hpp"
#include "AssaultTerminator.hpp"
#include <iostream>

int main(void){
  {
    ISpaceMarine* bob = new TacticalMarine;
    ISpaceMarine* jim = new AssaultTerminator;
    ISpaceMarine* null = NULL;
    Squad* vlc = new Squad;
    Squad vlc2;
    std::cout << "adding first unit -> " << vlc->push(bob) << std::endl;
    std::cout << "adding scnd unit fail -> " << vlc->push(bob) << std::endl;
    std::cout << "adding scnd unit -> " << vlc->push(jim) << std::endl;
    std::cout << "adding third NULL -> " << vlc->push(NULL) << std::endl;
    std::cout << "adding third NULL -> " << vlc->push(null) << std::endl;
    for (int i = 0; i < vlc->getCount(); ++i)
    {
      ISpaceMarine* cur = vlc->getUnit(i);
      cur->battleCry();
      cur->rangedAttack();
      cur->meleeAttack();
    }
    *vlc = vlc2;
    delete (vlc);
  }
  {
    ISpaceMarine* bob = new TacticalMarine;
    ISpaceMarine* jim = new AssaultTerminator;
    Squad* vlc = new Squad;
    vlc->push(bob);
    vlc->push(jim);
    ISquad* cpy = new Squad(*vlc);
    ISquad* cpyCpy = new Squad;
    *cpyCpy = *cpy;
    delete(vlc);
    delete(cpy);
    jim = new TacticalMarine();
    cpyCpy->push(jim);
    for (int i = 0; i < cpyCpy->getCount(); ++i)
    {
      ISpaceMarine* cur = cpyCpy->getUnit(i);
      cur->battleCry();
      cur->rangedAttack();
      cur->meleeAttack();
    }
    delete(cpyCpy);
  }
  while (1);
  return 0;
}

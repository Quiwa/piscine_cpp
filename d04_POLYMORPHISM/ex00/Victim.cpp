#include "Victim.hpp"
#include <iostream>
#include <string>

void Victim::getPolymorphed(void) const{
	std::cout << this->_name << " has been turned into a cute little sheep!" << std::endl;
}

/*
** CANONICAL FUNCS
*/
Victim::Victim()
:
_name("")
{
  std::cout << "Some random victim called " << this->_name << " just appeared!" << std::endl;
}

Victim::Victim(std::string name)
:
_name(name)
{
  std::cout << "Some random victim called " << this->_name << " just appeared!" << std::endl;
}

Victim::Victim(Victim const & src){
	*this = src;
  std::cout << "Some random victim called " << this->_name << " just appeared!" << std::endl;
	return;
}

Victim &	Victim::operator=(Victim const & rhs){
  this->_name = rhs.getName();
	return *this;
}

Victim::~Victim(void){
	std::cout << "Victim " << this->_name <<" just died for no apparent reason!" << std::endl;

  return;
}

/*
** ACCESSORS
*/
const std::string & Victim::getName(void) const{
  return (this->_name);
}

std::ostream & operator<<(std::ostream &os, Victim & victim){
  os << "I'm " << victim.getName() << " and I like otters!" << std::endl;
  return (os);
}

#ifndef VICTIM_H
# define VICTIM_H

#include <iostream>
#include <string>

class Victim{

public:

	virtual void getPolymorphed(void) const;

	/*
	** CANONICAL FUNCS
	*/
	Victim(void);
	Victim(std::string name);
	Victim(Victim const & src);
	virtual ~Victim(void);
	Victim & operator=(Victim const & rhs);

	/*
	** ACCESSORS
	*/
	const std::string & getName(void) const;

protected:
	std::string _name;



};

std::ostream & operator<<(std::ostream &os, Victim & sorcerer);

#endif

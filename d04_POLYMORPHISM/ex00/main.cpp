#include "Sorcerer.hpp"
#include "Peon.hpp"
#include "Cat.hpp"
#include "Dog.hpp"
#include "Victim.hpp"
#include <iostream>

int main()
{
  Sorcerer *robert = new Sorcerer("Robert", "the Magnificent");
  Sorcerer *cpy = new Sorcerer("John", "ta mere");
  Sorcerer *cpy2 = new Sorcerer(*cpy);
  *cpy = *robert;
  Victim jim("Jimmy");
  cpy2->polymorph(jim);
  delete(robert);
  delete(cpy);
  Peon joe("Joe");
  std::cout << cpy << jim << joe;
  cpy2->polymorph(jim);
  cpy2->polymorph(joe);

  Dog dog("Dog");
  Cat cat("Cat");
  cpy2->polymorph(dog);
  cpy2->polymorph(cat);
  delete(cpy2);
  return 0;
}

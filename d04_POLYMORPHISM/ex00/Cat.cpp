#include "Cat.hpp"
#include "Victim.hpp"
#include <iostream>
#include <string>

void Cat::getPolymorphed(void) const{
  std::cout << this->_name << " has been turned into a dark pony!" << std::endl;
}

Cat::Cat(std::string name)
:
Victim(name)
{
  std::cout << "Miaaaaouw." << std::endl;
	return;
}

Cat::Cat(void){
  std::cout << "Miaaaaouw." << std::endl;
	return;
}


Cat::Cat(Cat const & src){
  std::cout << "Miaaaaouw." << std::endl;
	*this = src;
	return;
}

Cat &	Cat::operator=(Cat const & rhs)
{
  Victim::operator=(rhs);
	return *this;
}

Cat::~Cat(void){
  std::cout << "Bleuark... maouw" << std::endl;
	return;
}

#ifndef DOG_H
# define DOG_H

#include <iostream>
#include <string>
#include "Victim.hpp"

class Dog : public Victim{

public:
	Dog(void);
	Dog(std::string);
	Dog(Dog const & src);
	virtual ~Dog(void);
	Dog & operator=(Dog const & rhs);
	virtual void getPolymorphed(void) const;
};

#endif

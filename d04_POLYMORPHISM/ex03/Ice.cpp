#include <iostream>
#include <string>
#include "Ice.hpp"
#include "ICharacter.hpp"
#include "AMateria.hpp"

void Ice::use(ICharacter& target){
	AMateria::use(target);
	std::cout << "* shoots an ice bolt at " << target.getName() << " *" << std::endl;
}

Ice* Ice::clone() const{
	Ice *ret = new Ice();
	*ret = *this;
	return (ret);
}

/*
** CANONICAL FUNCS
*/

Ice::Ice(void) : AMateria("ice"){
	return;
}

Ice::Ice(Ice const & src) : AMateria("ice"){
	*this = src;
	return;
}

Ice &	Ice::operator=(Ice const & rhs){
  setXP(rhs.getXP());
	return *this;
}

Ice::~Ice(void){
	return;
}

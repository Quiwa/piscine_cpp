#ifndef MATERIASOURCE_H
# define MATERIASOURCE_H

#include <iostream>
#include <string>
#include "IMateriaSource.hpp"
#include "AMateria.hpp"

class MateriaSource : public IMateriaSource{
  public:

    virtual AMateria* createMateria(std::string const & type);
    virtual void learnMateria(AMateria*);

    /*
    ** CANONICAL FUNCS
    */
  	MateriaSource(void);
  	MateriaSource(MateriaSource const & src);
  	~MateriaSource(void);
  	MateriaSource & operator=(MateriaSource const & rhs);

  private:
    AMateria *_materias[4];

};

#endif

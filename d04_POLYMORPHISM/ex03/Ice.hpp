#ifndef ICE_H
# define ICE_H

#include <iostream>
#include <string>
#include "AMateria.hpp"

class Ice : public AMateria{

public:

  virtual void use(ICharacter& target);
  virtual Ice* clone() const;

  /*
  ** CANONICAL FUNCS
  */
	Ice(void);
	Ice(Ice const & src);
	~Ice(void);
	Ice & operator=(Ice const & rhs);

private:

};

#endif

#ifndef CURE_H
# define CURE_H

#include <iostream>
#include <string>
#include "AMateria.hpp"

class Cure : public AMateria{

public:

  virtual void use(ICharacter& target);
  virtual Cure* clone() const;

  /*
  ** CANONICAL FUNCS
  */
	Cure(void);
	Cure(Cure const & src);
	~Cure(void);
	Cure & operator=(Cure const & rhs);

private:

};

#endif

#include "AMateria.hpp"
#include <iostream>

void AMateria::use(ICharacter& ){
  _xp += 10;
}

/*
** ACCESSORS
*/

std::string const & AMateria::getType() const{
  return (_type);
}

unsigned int AMateria::getXP() const{
  return (_xp);
}

void AMateria::setXP(unsigned int nb){
  _xp = nb;
}

/*
** CANONICAL FUNCS
*/
AMateria::AMateria(std::string const & type) : _type(type)
{
  _xp = 0;
}

AMateria::AMateria(void) : _type(""){
  _xp = 0;
}

AMateria::~AMateria(void){
}

AMateria & AMateria::operator=(AMateria const & rhs){
  _xp = rhs._xp;
  return (*this);
}

#include "Character.hpp"
#include "AMateria.hpp"
#include <string>

void Character::equip(AMateria* m){
  for (int i = 0; i < 5; i++)
    if (this->_mat[i] == NULL)
    {
      this->_mat[i] = m;
      return ;
    }
}

void Character::unequip(int idx){
  if (!doesMateriaIdxExist(idx))
    return ;
  this->_mat[idx] = NULL;
}

void Character::use(int idx, ICharacter& target){
  if (!doesMateriaIdxExist(idx))
    return ;
  this->_mat[idx]->use(target);
}

bool Character::doesMateriaIdxExist(int idx) const{
  return (!(idx >= 4 || idx < 0 || this->_mat[idx] == NULL));
}

/*
** ACCESSORS
*/
std::string const & Character::getName() const{
  return (this->_name);
}

Character::Character(std::string const &name) : _name(name)
{
  for (int i = 0; i < 4; i++) {
    this->_mat[i] = NULL;
  }
	return;
}

/*
** CANONICAL FUNCS
*/

Character::Character(Character & src){
  this->_name = src._name;
  for (size_t i = 0; i < 4; i++)
    this->_mat[i] = src._mat[i];
}

Character::~Character(void) {
  for (int i = 0; i < 4; i++)
    if (this->_mat[i] != NULL)
      delete(this->_mat[i]);
}

Character & Character::operator=(Character const & rhs){
  for (int i = 0; i < 4; i++)
  {
    if (this->_mat[i] != NULL)
      delete(this->_mat[i]);
    this->_mat[i] = rhs._mat[i];
  }
  this->_name = rhs._name;
  return (*this);
}

#include "FragTrap.hpp"
#include <iostream>
#include <string>
#include <sstream>

FragTrap::FragTrap(std::string name) :
					_name(name), _hitPoints(100),
					_maxHitPoints(100), _energyPoints(100),
					_maxEnergyPoints(100),
					_meleeAttackDamage(30), _rangedAttackDamage(20),
					_armorDamageReduction(5){
	std::cout << "<FR4G-TP (" << name <<") has been created> 'Are you God ? Am I dead ?' " << std::endl << std::endl;
	this->_level = 1;
	this->_attacks[0] = &FragTrap::jujitsuAttack;
	this->_attacks[1] = &FragTrap::karateAttack;
	this->_attacks[2] = &FragTrap::distanceAttack;
	this->_attacks[3] = &FragTrap::coolAttack;
	this->_attacks[4] = &FragTrap::effectiveAttack;
	return;
}

FragTrap::FragTrap(FragTrap const & src){
	*this = src;
	std::cout << "<FR4G-TP (" << _name << ") has been created> 'Are you God ? Am I dead ?' " << std::endl << std::endl;
	return;
}

FragTrap &	FragTrap::operator=(FragTrap const & rhs){
	this->_name = rhs.getName();
	this->_hitPoints = rhs.getHitPoints();
	this->_maxHitPoints = rhs.getMaxHitPoints();
	this->_energyPoints = rhs.getEnergyPoints();
	this->_maxEnergyPoints = rhs.getMaxEnergyPoints();
	this->_level = rhs.getLevel();
	this->_meleeAttackDamage = rhs.getMeleeAttackDamage();
	this->_rangedAttackDamage = rhs.getRangedAttackDamage();
	this->_armorDamageReduction = rhs.getArmorDamageReduction();
	this->_attacks[0] = rhs._attacks[0];
	this->_attacks[1] = rhs._attacks[1];
	this->_attacks[2] = rhs._attacks[2];
	this->_attacks[3] = rhs._attacks[3];
	this->_attacks[4] = rhs._attacks[4];
	return *this;
}

int FragTrap::getAmountOfDamages(int amount){
	return ((amount >= _armorDamageReduction) ? amount - _armorDamageReduction : 0);
}

void FragTrap::rangedAttack(std::string const & target){
	std::ostringstream damages;
	damages << getRangedAttackDamage();
	std::cout << "<FR4G-TP (" + this->_name + ")> attacks " + target + " at range, causing " +  damages.str() + " points of damage!" << std::endl;
}

void FragTrap::meleeAttack(std::string const & target){
	std::ostringstream damages;
	damages << getMeleeAttackDamage();
	std::cout << "<FR4G-TP (" + this->_name + ")> attacks " + target + " at melee, causing " +  damages.str() + " points of damage!" << std::endl;
}

void FragTrap::takeDamage(unsigned int amount){
	std::ostringstream damages;
	damages << getAmountOfDamages(amount);
	std::cout << "<FR4G-TP (" + this->_name + ")> recieves " +  damages.str() + " points of damage!" << std::endl;
	setHitPoints(this->_hitPoints - amount);
}

void FragTrap::beRepaired(unsigned int amount){
	std::ostringstream damages;
	damages << amount;
	std::cout << "<FR4G-TP (" + this->_name + ")> is repaired by " +  damages.str() + " points!" << std::endl;
	setHitPoints(this->_hitPoints + amount);
}

void FragTrap::vaulthunter_dot_exe(std::string const & target){
	if (this->getEnergyPoints() < 25)
	{
		std::cout << "<FR4G-TP (" + this->_name + ") is out of energy." << std::endl << std::endl;
		return ;
	}
	(this->*_attacks[std::rand() % 5])(target);
	this->setEnergyPoints(this->_energyPoints - 25);
	std::cout << std::endl;
}

void FragTrap::jujitsuAttack(std::string target){
	std::cout  << "<FR4G-TP (" + this->_name + ") attempt a ju-jitsu attack on " << target << " but he doesn't know any ju-jitsu.." << std::endl << "  => He losts 25 energy point for nothing." << std::endl;
}

void FragTrap::karateAttack(std::string target){
	std::cout  << "<FR4G-TP (" + this->_name + ") attempt a karate attack on but " << target << " is better at Karate.." << std::endl << "  => " <<this->_name << " losts 10 HP." << std::endl;
	this->setHitPoints(this->_hitPoints - 10);
}

void FragTrap::distanceAttack(std::string target){
	std::cout  << "<FR4G-TP (" + this->_name + ") attempt a distance attack on " << target << " but he doesn't have any weapon." << std::endl <<  "  => He losts 25 energy point for waving in the air." << std::endl;
}

void FragTrap::coolAttack(std::string target){
	std::cout  << "<FR4G-TP (" + this->_name + ") attempt a cool attack on " << target << " .." << std::endl <<  "It's f*cking impressive." << std::endl <<  "  => Such a shame that he doesn't know how to aim." << std::endl;
	this->meleeAttack(target);
}

void FragTrap::effectiveAttack(std::string target){
	std::cout  << "<FR4G-TP (" + this->_name + ") attempt something on " << target << " .." << std::endl <<  " and it actually WORKED." << std::endl;
	this->meleeAttack(target);
}

FragTrap::~FragTrap(){
	std::cout << "<FR4G-TP (" << this->_name << ") is dead... it's getting risky to open doors nowadays !" << std::endl;
	return;
}

const std::string & FragTrap::getName() const{
	return (this->_name);
}

int FragTrap::getHitPoints() const{
	return (this->_hitPoints);
}

int FragTrap::getMaxHitPoints() const{
	return (this->_maxHitPoints);
}

int FragTrap::getEnergyPoints() const{
	return (this->_energyPoints);
}

int FragTrap::getMaxEnergyPoints() const{
	return (this->_maxEnergyPoints);
}

int FragTrap::getLevel() const{
	return (this->_level);
}

int FragTrap::getMeleeAttackDamage() const{
	return (this->_meleeAttackDamage);
}

int FragTrap::getRangedAttackDamage() const{
	return (this->_rangedAttackDamage);
}

int FragTrap::getArmorDamageReduction() const{
	return (this->_armorDamageReduction);
}

void FragTrap::setEnergyPoints(int val){
	if (val < 0)
		this->_energyPoints = 0;
	else if (val > this->_maxHitPoints)
		this->_energyPoints = this->_maxEnergyPoints;
	else
		this->_energyPoints = val;
}

void FragTrap::setHitPoints(int val){
	if (val < 0)
		this->_hitPoints= 0;
	else if (val > this->_maxHitPoints)
		this->_hitPoints = this->_maxHitPoints;
	else
		this->_hitPoints = val;
	}

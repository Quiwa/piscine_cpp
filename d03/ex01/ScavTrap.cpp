#include "ScavTrap.hpp"
#include <iostream>
#include <string>

ScavTrap::ScavTrap(void){
	return ;
}

ScavTrap::ScavTrap(std::string name) :
					_name(name), _hitPoints(100),
					_maxHitPoints(100), _energyPoints(50),
					_maxEnergyPoints(50),
					_meleeAttackDamage(20), _rangedAttackDamage(15),
					_armorDamageReduction(3){
	std::cout << "<SC4V-TP (" << _name <<") has been created> 'Not gonna jam the door this time !' " << std::endl << std::endl;
	this->_level = 1;
	this->_challenges[0] = &ScavTrap::challengePolitely;
	this->_challenges[1] = &ScavTrap::challengeArchly;
	this->_challenges[2] = &ScavTrap::challengeSneaky;
	this->_challenges[3] = &ScavTrap::challengeBug;
	this->_challenges[4] = &ScavTrap::challengeMadly;
	return;
}

ScavTrap::ScavTrap(ScavTrap const & src){
	*this = src;
	std::cout << "<SC4V-TP (" << _name <<") has been created> 'Not gonna jam the door this time !' " << std::endl << std::endl;
	return;
}

ScavTrap &	ScavTrap::operator=(ScavTrap const & rhs){
	this->_name = rhs.getName();
	this->_hitPoints = rhs.getHitPoints();
	this->_maxHitPoints = rhs.getMaxHitPoints();
	this->_energyPoints = rhs.getEnergyPoints();
	this->_maxEnergyPoints = rhs.getMaxEnergyPoints();
	this->_level = rhs.getLevel();
	this->_meleeAttackDamage = rhs.getMeleeAttackDamage();
	this->_rangedAttackDamage = rhs.getRangedAttackDamage();
	this->_armorDamageReduction = rhs.getArmorDamageReduction();
	for (size_t i = 0; i < 5; i++)
		this->_challenges[i] = rhs._challenges[i];
	return *this;
}

void ScavTrap::rangedAttack(std::string const & target){
	std::cout << "<SC4V-TP (" + this->_name + ")> attacks " + target + " at range, causing " +  std::to_string(this->getRangedAttackDamage()) + " points of damage!" << std::endl;
}

void ScavTrap::meleeAttack(std::string const & target){
	std::cout << "<SC4V-TP (" + this->_name + ")> attacks " + target + " at melee, causing " +  std::to_string(this->getMeleeAttackDamage()) + " points of damage!" << std::endl;
}

void ScavTrap::setEnergyPoints(int val){
	if (val < 0)
		this->_energyPoints = 0;
	else if (val > this->_maxHitPoints)
		this->_energyPoints = this->_maxEnergyPoints;
	else
		this->_energyPoints = val;
}

void ScavTrap::setHitPoints(int val){
	if (val < 0)
		this->_hitPoints= 0;
	else if (val > this->_maxHitPoints)
		this->_hitPoints = this->_maxHitPoints;
	else
		this->_hitPoints = val;
}

int ScavTrap::getAmountOfDamages(int amount){
	return ((amount >= _armorDamageReduction) ? amount - _armorDamageReduction : 0);
}

void ScavTrap::takeDamage(unsigned int amount){
	std::cout << "<SC4V-TP (" + this->_name + ")> recieves " +  std::to_string(amount) + " points of damage, but is still swaggy asf" << std::endl;
	amount = getAmountOfDamages(amount);
	setHitPoints(this->_hitPoints - amount);
}

void ScavTrap::beRepaired(unsigned int amount){
	std::cout << "<SC4V-TP (" + this->_name + ")> is repaired by " +  std::to_string(amount) + " points ! Never gonna kill that f*cker" << std::endl;
	setHitPoints(this->_hitPoints + amount);
}

void ScavTrap::challengeNewcomer(std::string const & target){
	if (this->getEnergyPoints() == 0)
	{
		std::cout << this->_name << " is out of energy... But still embody swganess." << std::endl << std::endl;
		return ;
	}
	(this->*_challenges[std::rand() % 5])(target);
	std::cout << std::endl;
}

void ScavTrap::challengePolitely(std::string target){
	std::cout << "Hi " << target << ", feel free to enter at our own risk" << std::endl;
}

void ScavTrap::challengeArchly(std::string){
	std::cout << "Do I try to enter your devil lair ? DO I ? Get the f*ck off" << std::endl;
}

void ScavTrap::challengeSneaky(std::string target){
	std::cout << "Hi " << target << ", sure come ! * activates an awful trap behind the door *" << std::endl;
}

void ScavTrap::challengeBug(std::string){
	std::cout << "Update 1.0.222 is now available for your SC4V-TP" << std::endl;
}

void ScavTrap::challengeMadly(std::string target){
	std::cout << "Hi " << target << ", AYAAAAAH hahahehehehehe *cough cough* ahem" << std::endl;
}

std::string ScavTrap::getName() const{
	return (this->_name);
}

int ScavTrap::getHitPoints() const{
	return (this->_hitPoints);
}

int ScavTrap::getMaxHitPoints() const{
	return (this->_maxHitPoints);
}

int ScavTrap::getEnergyPoints() const{
	return (this->_energyPoints);
}

int ScavTrap::getMaxEnergyPoints() const{
	return (this->_maxEnergyPoints);
}

int ScavTrap::getLevel() const{
	return (this->_level);
}

int ScavTrap::getMeleeAttackDamage() const{
	return (this->_meleeAttackDamage);
}

int ScavTrap::getRangedAttackDamage() const{
	return (this->_rangedAttackDamage);
}

int ScavTrap::getArmorDamageReduction() const{
	return (this->_armorDamageReduction);
}


ScavTrap::~ScavTrap(){
	std::cout << "<SC4V-TP (" << _name <<") is dead> 'Is that cool? That's... what 'cool' is?' " << std::endl << std::endl;
	return;
}

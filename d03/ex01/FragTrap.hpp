#ifndef FRAGTRAP_H
# define FRAGTRAP_H

#include <iostream>
#include <string>

class FragTrap{

public:
	FragTrap(void);
	FragTrap(std::string name);
	FragTrap(FragTrap const & src);
	~FragTrap(void);
	FragTrap & operator=(FragTrap const & rhs);

  void rangedAttack(std::string const & target);
  void meleeAttack(std::string const & target);
  void takeDamage(unsigned int amount);
  void beRepaired(unsigned int amount);

	void vaulthunter_dot_exe(std::string const & target);

  const std::string & getName() const;
  int getHitPoints() const;
  int getMaxHitPoints() const;
  int getEnergyPoints() const;
	int getLevel() const;
  int getMaxEnergyPoints() const;
  int getRangedAttackDamage() const;
  int getMeleeAttackDamage() const;
  int getArmorDamageReduction() const;

	void jujitsuAttack(std::string target);
	void karateAttack(std::string target);
	void distanceAttack(std::string target);
	void coolAttack(std::string target);
	void effectiveAttack(std::string target);


private:
  std::string _name;
  int _hitPoints;
  int _maxHitPoints;
  int _energyPoints;
  int _maxEnergyPoints;
  int _level;
  int _meleeAttackDamage;
  int _rangedAttackDamage;
  int _armorDamageReduction;

	void (FragTrap::*_attacks[5])(std::string target);
	void setEnergyPoints(int val);
	void setHitPoints(int val);
	int getAmountOfDamages(int amount);
};

#endif

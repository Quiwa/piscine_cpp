#include "FragTrap.hpp"
#include "ScavTrap.hpp"

int main(void){
  FragTrap ExhaustedOne("ExhaustedOne");
  ScavTrap JohnTucker("John Tucker");

  for (size_t i = 0; i < 6; i++) {
	   std::srand(std::time(0) + i);
    ExhaustedOne.vaulthunter_dot_exe("You");
  }

  for (size_t i = 0; i < 6; i++) {
	   std::srand(std::time(0) + i);
    JohnTucker.challengeNewcomer("You");
  }

  ScavTrap *Original = new ScavTrap("John Tucker");
  ScavTrap *cpy = new ScavTrap(*Original);
  ScavTrap *cpycpy = new ScavTrap("cpycpy");

  delete(Original);
  *cpycpy = *cpy;

  for (size_t i = 0; i < 6; i++) {
	   std::srand(std::time(0) + i);
    cpy->challengeNewcomer("You");
  }
  delete(cpy);

  for (size_t i = 0; i < 6; i++) {
	   std::srand(std::time(0) + i);
    cpycpy->challengeNewcomer("You");
  }
  delete(cpycpy);

  return (0);
}

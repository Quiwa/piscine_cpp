#include "NinjaTrap.hpp"
#include "ClapTrap.hpp"
#include "FragTrap.hpp"
#include "ScavTrap.hpp"
#include <iostream>
#include <string>

NinjaTrap::NinjaTrap(void){
	return ;
}

NinjaTrap::NinjaTrap(std::string name)
:
	ClapTrap(name, 60, 60, 120, 120, 60, 5, 0, 1)
{
	std::cout << "NinjaTrap constructor called" << std::endl;
	return;
}

NinjaTrap::NinjaTrap(NinjaTrap const & src)
:
ClapTrap(src)
{
	std::cout << "NinjaTrap copy constructor called" << std::endl;
	*this = src;
	return;
}

void NinjaTrap::ninjaShoebox(FragTrap & target){
	std::string msg = this->getName() + " goes for FragTrap " + target.getName() + ". "
	+ this->getName() + " is way too fast for this huge garbage !";
	meleeAttack(msg);
	target.setHitPoints(target.getHitPoints() + target.getArmorDamageReduction() - this->getMeleeAttackDamage());
	std::cout << std::endl;
}

void NinjaTrap::ninjaShoebox(ScavTrap & target){
	std::string msg = this->getName() + " goes for ScavTrap " + target.getName() + ". "
	+ this->getName() + " is quite fast but scav trap is small and agile !";
	meleeAttack(msg);
	target.setHitPoints(target.getHitPoints() + target.getArmorDamageReduction() - this->getMeleeAttackDamage());
	std::cout << std::endl;
}

void NinjaTrap::ninjaShoebox(NinjaTrap & target){
	std::string msg = this->getName() + " goes for NinjaTrap " + target.getName() + ". "
	+ " They're both so fast they can't see each other !";
	meleeAttack(msg);
	std::cout << std::endl;
}

NinjaTrap & NinjaTrap::operator=(NinjaTrap const & rhs)
{
	ClapTrap::operator=(rhs);
	return *this;
}

NinjaTrap::~NinjaTrap(){
	std::cout << "NinjaTrap destructor called" << std::endl;
	return;
}

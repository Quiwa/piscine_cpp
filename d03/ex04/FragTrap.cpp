#include "FragTrap.hpp"
#include "ClapTrap.hpp"
#include <iostream>
#include <string>

FragTrap::FragTrap(void){
	return ;
}

FragTrap::FragTrap(std::string name)
:
	ClapTrap(name, 100, 100, 100, 100, 30, 20, 5, 1)
{
	std::cout << "FragTrap constructor called" << std::endl;
	this->_challenges[0] = &FragTrap::jujitsuAttack;
	this->_challenges[1] = &FragTrap::karateAttack;
	this->_challenges[2] = &FragTrap::distanceAttack;
	this->_challenges[3] = &FragTrap::coolAttack;
	this->_challenges[4] = &FragTrap::effectiveAttack;
	return;
}

FragTrap::FragTrap(FragTrap const & src)
:
ClapTrap(src)
{
	std::cout << "FragTrap copy constructor called" << std::endl;
	*this = src;
	return;
}

FragTrap & FragTrap::operator=(FragTrap const & rhs)
{
	ClapTrap::operator=(rhs);
	for (size_t i = 0; i < 5; i++)
		this->_challenges[i] = rhs._challenges[i];
	return *this;
}

void FragTrap::vaulthunter_dot_exe(std::string const & target){
	int attackNb;

	if (this->getEnergyPoints() == 0)
	{
		std::cout << this->getName() << " is out of energy." << std::endl << std::endl;
		return ;
	}
	attackNb = std::rand() % 5;
	(this->*_challenges[attackNb])(target);
	std::cout << std::endl;
}

void FragTrap::jujitsuAttack(std::string target){
	std::string msg = this->_name + " attempt a ju-jitsu attack on " + target + " ..";
	this->meleeAttack(msg);
}

void FragTrap::karateAttack(std::string target){
	std::string msg = this->_name + " attempt a karate attack on " + target + " ..";
	this->meleeAttack(msg);
}

void FragTrap::distanceAttack(std::string target){
	std::string msg = this->_name + " attempt a distance attack on " + target + " .." +  "but he doesn't have any weapon." +  "  => He losts 25 energy point for waving in the air.";
	this->rangedAttack(msg);
}

void FragTrap::coolAttack(std::string target){
	std::string msg = this->_name + " attempt a cool attack on " + target + " .." +  "It's f*cking impressive." +  "  => Such a shame that he doesn't know how to aim.";
	this->meleeAttack(msg);
}

void FragTrap::effectiveAttack(std::string target){
	std::string msg = this->_name + " attempt something on " + target + " .." +  " and it actually WORKED." +  "  => He gains one level";
	this->meleeAttack(msg);
}

FragTrap::~FragTrap(){
	std::cout << "FlagTRap destructor called" << std::endl;
	return;
}

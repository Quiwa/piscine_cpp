#ifndef SUPERTRAP_H
# define SUPERTRAP_H

#include <iostream>
#include <string>
#include "FragTrap.hpp"
#include "ScavTrap.hpp"

class SuperTrap : public NinjaTrap, public FragTrap{

public:
	SuperTrap(void);
	SuperTrap(std::string name);
	SuperTrap(SuperTrap const & src);
	~SuperTrap(void);
	SuperTrap & operator=(SuperTrap const & rhs);
};

#endif

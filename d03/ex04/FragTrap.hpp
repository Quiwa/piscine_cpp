#ifndef FRAGTRAP_H
# define FRAGTRAP_H

#include <iostream>
#include <string>
#include "ClapTrap.hpp"

class FragTrap : virtual public ClapTrap{

public:
	FragTrap(void);
	FragTrap(std::string name);
	FragTrap(FragTrap const & src);
	~FragTrap(void);
	FragTrap & operator=(FragTrap const & rhs);

	void vaulthunter_dot_exe(std::string const & target);

	void jujitsuAttack(std::string target);
	void karateAttack(std::string target);
	void distanceAttack(std::string target);
	void coolAttack(std::string target);
	void effectiveAttack(std::string target);


private:

	void (FragTrap::*_challenges[5])(std::string target);
};

#endif

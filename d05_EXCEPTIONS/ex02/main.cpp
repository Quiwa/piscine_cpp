#include "ShrubberyCreationForm.hpp"
#include "RobotomyRequestForm.hpp"
#include "PresidentialPardonForm.hpp"
#include "Bureaucrat.hpp"
#include "Form.hpp"

Bureaucrat * tryBureaucratCreation(std::string name, unsigned int grade);
void tryFormExecution(Form * form, Bureaucrat bureaucrat);
void tryFormSigning(Form & form, Bureaucrat & bureaucrat);
void tryBureaucratFormExecution(Form * form, Bureaucrat bureaucrat);

int main(void){

  Bureaucrat * Bob = tryBureaucratCreation("Bob", 1);
  Bureaucrat * James = tryBureaucratCreation("James", 150);

  Form * shrubberyForm = new ShrubberyCreationForm("home");
  Form * robotomyForm = new RobotomyRequestForm("Trump");
  Form * presidentialPardon = new PresidentialPardonForm("Obama");

  std::cout << " -- INITIAL STATES --" << std::endl;
  std::cout << *Bob;
  std::cout << *James;
  std::cout << *shrubberyForm;
  std::cout << *robotomyForm;
  std::cout << *presidentialPardon;

  std::cout << std::endl;

  std::cout << " -- FORM EXECUTION ON UNSIGNED FORMS --" << std::endl;
  tryFormExecution(shrubberyForm, *Bob);
  tryFormExecution(robotomyForm, *Bob);
  tryFormExecution(presidentialPardon, *Bob);

  std::cout << std::endl;

  std::cout << " -- SIGNATURE FAILURES --" << std::endl;
  tryFormSigning(*shrubberyForm, *James);
  std::cout << *shrubberyForm;
  tryFormSigning(*robotomyForm, *James);
  std::cout << *robotomyForm;
  tryFormSigning(*presidentialPardon, *James);
  std::cout << *presidentialPardon;

  std::cout << std::endl;

  std::cout << " -- SIGNATURE SUCCESSES --" << std::endl;
  tryFormSigning(*shrubberyForm, *Bob);
  std::cout << *shrubberyForm;
  tryFormSigning(*robotomyForm, *Bob);
  std::cout << *robotomyForm;
  tryFormSigning(*presidentialPardon, *Bob);
  std::cout << *presidentialPardon;

  std::cout << std::endl;

  std::cout << " -- FORM ACTION FAILURES --" << std::endl;
  tryFormExecution(shrubberyForm, *James);
  tryFormExecution(robotomyForm, *James);
  tryFormExecution(presidentialPardon, *James);

  std::cout << std::endl;

  std::cout << " -- FORM ACTION SUCCESSES --" << std::endl;
  tryFormExecution(shrubberyForm, *Bob);
  tryFormExecution(robotomyForm, *Bob);
  tryFormExecution(presidentialPardon, *Bob);

  std::cout << " -- FORM EXECUTION FAILURES --" << std::endl;
  tryBureaucratFormExecution(shrubberyForm, *Bob);
  tryBureaucratFormExecution(robotomyForm, *Bob);
  tryBureaucratFormExecution(presidentialPardon, *Bob);

  return 0;
}

Bureaucrat * tryBureaucratCreation(std::string name, unsigned int grade){

  Bureaucrat * bureaucrat = NULL;

  try{
    bureaucrat = new Bureaucrat(name, grade);
    return (bureaucrat);
  }
  catch (Bureaucrat::GradeTooLowException e){
    std::cout << name << " " << e.what() << std::endl;
  }
  catch (Bureaucrat::GradeTooHighException e){
    std::cout << name << " " << e.what() << std::endl;
  }
  return (bureaucrat);
}

void tryBureaucratFormExecution(Form * form, Bureaucrat bureaucrat){
  try{bureaucrat.executeForm(*form);}
  catch (Bureaucrat::GradeTooLowException e){std::cout << e.what() << std::endl;}
  catch (Form::GradeTooLowException e){std::cout << e.what() << std::endl;}
  catch (Form::FormNotSignedException e){std::cout << e.what() << std::endl;}
}

void tryFormExecution(Form * form, Bureaucrat bureaucrat)
{
  try{form->execute(bureaucrat);}
  catch (Bureaucrat::GradeTooLowException e){std::cout << e.what() << std::endl;}
  catch (Form::GradeTooLowException e){std::cout << e.what() << std::endl;}
  catch (Form::FormNotSignedException e){std::cout << e.what() << std::endl;}
}

void tryFormSigning(Form & form, Bureaucrat &bureaucrat){
  try{bureaucrat.signForm(form);}
  catch (Form::GradeTooLowException e){std::cout << e.what() << std::endl;}
  catch (Bureaucrat::GradeTooLowException e){std::cout << e.what() << std::endl;}
  catch (Form::FormNotSignedException e){std::cout << e.what() << std::endl;}
}

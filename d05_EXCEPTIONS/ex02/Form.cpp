#include "Form.hpp"
#include "Bureaucrat.hpp"
#include <iostream>
#include <string>

const char * Form::GradeTooLowException::what() const throw(){
	return ("Grade is too low.");
}

const char * Form::GradeTooHighException::what() const throw(){
	return ("Grade is too high.");
}

const char * Form::FormNotSignedException::what() const throw(){
	return ("Form is not signed");
}

void Form::action(void) const{
}

void Form::execute(Bureaucrat const & executor) const{
	if (_isSigned && executor.getGrade() <= _toExecuteGrade)
		action();
	else if (!_isSigned)
		throw Form::FormNotSignedException();
	else
		throw Form::GradeTooLowException();
}

void Form::beSigned(Bureaucrat *bureaucrat){
  if (bureaucrat->getGrade() > _toSignGrade)
    throw Form::GradeTooLowException();
	_isSigned = true;
}

/*
** ACCESSORS
*/

unsigned int Form::getToSignGrade(void) const{
  return (_toSignGrade);
}

unsigned int Form::getToExecuteGrade(void) const{
  return (_toExecuteGrade);
}

const std::string & Form::getName(void) const{
  return (_name);
}

bool Form::getIsSigned(void) const{
  return (_isSigned);
}

/*
** Constructor
*/
Form::Form(std::string name, unsigned int signGrade, unsigned int execGrade)
:
_name(name),
_toSignGrade(signGrade),
_toExecuteGrade(execGrade),
_isSigned(false)
{
	if (_toExecuteGrade == 0 || _toSignGrade == 0)
		throw Form::GradeTooHighException();
	else if (_toExecuteGrade > 150 || _toSignGrade > 150)
		throw Form::GradeTooLowException();
	return;
}

/*
** CANONICAL FUNCS
*/
Form::Form(void) : _name(""), _toSignGrade(1), _toExecuteGrade(1), _isSigned(false){
	return;
}

Form::Form(Form const & src) : _name(src._name), _toSignGrade(src._toSignGrade), _toExecuteGrade(src._toExecuteGrade){
	*this = src;
	return;
}

Form &	Form::operator=(Form const & rhs){
  this->_isSigned = rhs._isSigned;
	return *this;
}

Form::~Form(void){
	return;
}

std::ostream& operator<<(std::ostream& os, const Form & rhs){
  if (rhs.getIsSigned() == true)
    os << "<" << rhs.getName() << ">, file to sign grade <" << rhs.getToSignGrade() << "> and to execute grade <" << rhs.getToExecuteGrade() << "> is signed" << std::endl;
  else
    os << "<" << rhs.getName() << ">, file to sign grade <" << rhs.getToSignGrade() << "> and to exectute grade <" << rhs.getToExecuteGrade() << "> is not signed" << std::endl;
	return (os);
}

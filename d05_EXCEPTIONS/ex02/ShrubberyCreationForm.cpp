#include "Form.hpp"
#include "ShrubberyCreationForm.hpp"
#include <iostream>
#include <string>
#include <fstream>


ShrubberyCreationForm::ShrubberyCreationForm(std::string path) :
Form("Shrubbery form", 145, 137),
_path(path)
{
}

void ShrubberyCreationForm::action(void) const{
  std::ofstream file;
  file.open(_path + "_shrubbery");
  file << "           ; ; ;" << std::endl;
  file << "       ;        ;  ;     ;;    ;" << std::endl;
  file << "    ;                 ;         ;  ;" << std::endl;
  file << "                    ;" << std::endl;
  file << "                   ;                ;;" << std::endl;
  file << "   ;          ;            ;              ;" << std::endl;
  file << "   ;            ';,        ;               ;" << std::endl;
  file << "   ;              'b      *" << std::endl;
  file << "    ;              '$    ;;                ;;" << std::endl;
  file << "   ;    ;           $:   ;:               ;" << std::endl;
  file << " ;;      ;  ;;      *;  @):        ;   ; ;" << std::endl;
  file << "              ;     :@,@):   ,;**:'   ;" << std::endl;
  file << "  ;      ;,         :@@*: ;;**'      ;   ;" << std::endl;
  file << "           ';o;    ;:(@';@*''  ;" << std::endl;
  file << "   ;  ;       'bq,;;:,@@*'   ,*      ;  ;" << std::endl;
  file << "              ,p$q8,:@)'  ;p*'      ;" << std::endl;
  file << "       ;     '  ; '@@Pp@@*'    ;  ;" << std::endl;
  file << "        ;  ; ;;    Y7'.'     ;  ;" << std::endl;
  file << "                  :@):." << std::endl;
  file << "                 .:@:'." << std::endl;
  file << "               .::(@:.      " << std::endl;
  file.close();
}

/*
** CANONICAL FUNCS
*/
ShrubberyCreationForm::ShrubberyCreationForm(void) :
Form("Shrubbery form", 145, 137),
_path("")
{
	return;
}

ShrubberyCreationForm::ShrubberyCreationForm(ShrubberyCreationForm const & src):
Form::Form(src)
{
	*this = src;
	return;
}

ShrubberyCreationForm &	ShrubberyCreationForm::operator=(ShrubberyCreationForm const & rhs){
  Form::operator=(rhs);
  this->_path = rhs._path;
	return *this;
}

ShrubberyCreationForm::~ShrubberyCreationForm(void){
	return;
}

#ifndef BUREAUCRAT_H
# define BUREAUCRAT_H

#include <iostream>
#include <string>

class Bureaucrat{

public:
	class GradeTooLowException : public std::exception{
		public:
			virtual const char * what() const throw();
	};

	class GradeTooHighException : public std::exception{
		public:
			virtual const char * what() const throw();
	};

	void incrementGrade(void);
	void decrementGrade(void);

	/*
	** ACCESSORS
	*/
	unsigned int getGrade(void) const;
	std::string getName(void) const;

	Bureaucrat(std::string name, unsigned int grade);

	/*
	** CANONICAL FUNCS
	*/
	Bureaucrat(Bureaucrat const & src);
	~Bureaucrat(void);
	Bureaucrat & operator=(Bureaucrat const & rhs);

private:
	const std::string _name;
	unsigned int _grade;

	Bureaucrat(void);
	/*
	** ACCESSORS
	*/
	void setGrade(unsigned int grade);

};

std::ostream& operator<<(std::ostream& os, const Bureaucrat & dt);

#endif

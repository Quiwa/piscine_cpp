#include "Form.hpp"
#include "PresidentialPardonForm.hpp"
#include <iostream>
#include <string>
#include <time.h>


PresidentialPardonForm::PresidentialPardonForm(std::string path) :
Form("Presidential pardon form", 25, 5),
_path(path)
{
}

void PresidentialPardonForm::action(void) const{
  std::cout << "<"<< _path << "> has been pardoned by Zafod Beeblebrox" << std::endl;
}

/*
** CANONICAL FUNCS
*/
PresidentialPardonForm::PresidentialPardonForm(void) :
Form("Presidential pardon form", 25, 5)
{
	return;
}

PresidentialPardonForm::PresidentialPardonForm(PresidentialPardonForm const & src):
Form::Form(src)
{
	*this = src;
	return;
}

PresidentialPardonForm &	PresidentialPardonForm::operator=(PresidentialPardonForm const & rhs){
  Form::operator=(rhs);
  this->_path = rhs._path;
	return *this;
}

PresidentialPardonForm::~PresidentialPardonForm(void){
	return;
}

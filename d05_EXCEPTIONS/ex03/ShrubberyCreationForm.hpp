#ifndef SHRUBBERYCREATIONFORM_H
# define SHRUBBERYCREATIONFORM_H

#include <iostream>
#include <string>
#include "Form.hpp"

class ShrubberyCreationForm : public Form{

public:

	ShrubberyCreationForm(std::string path);

	/*
	** CANONICAL FUNCS
	*/
	ShrubberyCreationForm(ShrubberyCreationForm const & src);
	virtual ~ShrubberyCreationForm(void);
	ShrubberyCreationForm & operator=(ShrubberyCreationForm const & rhs);

private:
  std::string _path;

	ShrubberyCreationForm(void);
	virtual void action(void) const;

};

#endif

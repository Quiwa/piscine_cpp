#include "Intern.hpp"
#include "Form.hpp"
#include "PresidentialPardonForm.hpp"
#include "ShrubberyCreationForm.hpp"
#include "RobotomyRequestForm.hpp"
#include <iostream>
#include <string>


const char * Intern::UnknownForm::what() const throw(){
  return "EXCEPTION Unknown form";
}

Form * Intern::makePresidential(std::string name){
  return (new PresidentialPardonForm(name));
}

Form * Intern::makeRobot(std::string name){
  return (new RobotomyRequestForm(name));
}

Form * Intern::makeShrubbery(std::string name){
  return (new ShrubberyCreationForm(name));
}

Form * Intern::makeForm(std::string const & formType, std::string const & formTarget) const{
  for (size_t i = 0; i < 3; i++)
    if (formType == _formTypes[i].tagName)
      return (_formTypes[i].make(formTarget));
  throw UnknownForm();
  return (NULL);
}

/*
** CANONICAL FUNCS
*/
Intern::Intern(void){

  _formTypes[0].tagName = "presidential";
  _formTypes[0].make = &Intern::makePresidential;
  _formTypes[1].tagName = "robot";
  _formTypes[1].make = &Intern::makeRobot;
  _formTypes[2].tagName = "shrubbery";
  _formTypes[2].make = &Intern::makeShrubbery;
	return;
}

Intern::Intern(Intern const & src){
	*this = src;
	return;
}

Intern &	Intern::operator=(Intern const & rhs){
  (void)rhs;
	return *this;
}

Intern::~Intern(void){
	return;
}

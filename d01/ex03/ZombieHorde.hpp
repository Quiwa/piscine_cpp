#ifndef ZOMBIE_HORDE_H
# define ZOMBIE_HORDE_H

#include "Zombie.hpp"
#include <string>

class ZombieHorde{

public:
  ZombieHorde(int n);
  ~ZombieHorde(void);
  void announce(void) const;

private:
  Zombie * zombieHorde;
  std::string getRandomName(int) const;
  int n;
};

#endif

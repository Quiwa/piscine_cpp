#include "Zombie.hpp"
#include "ZombieHorde.hpp"

int main(void)
{
  ZombieHorde * zombieHorde = new ZombieHorde(100);
  ZombieHorde * zombieHordeB = new ZombieHorde(0);
  ZombieHorde * zombieHordeC = new ZombieHorde(-1);

  zombieHorde->announce();
  zombieHordeB->announce();
  zombieHordeC->announce();

  delete(zombieHorde);
  delete(zombieHordeB);
  delete(zombieHordeC);
}

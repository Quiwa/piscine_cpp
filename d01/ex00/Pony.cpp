#include "Pony.hpp"
#include <string>
#include <iostream>

Pony::Pony(std::string name){
  std::cout << "Pony <" << name << "> was born" <<std::endl;
  this->setName(name);
}

Pony::~Pony(){
  std::cout << "Pony <" << this->getName() << "> just died" <<std::endl;
}

void Pony::setName(const std::string & name){
  this->name = name;
}

const std::string & Pony::getName(void) const{
  return (this->name);
}

void Pony::run() const{
  std::cout << "Pony <" << this->getName() << "> is running ! " << std::endl;
}

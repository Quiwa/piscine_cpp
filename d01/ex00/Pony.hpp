#ifndef PONY_H
# define PONY_H

#include <string>

class Pony{

public:
  Pony(std::string name);
  ~Pony(void);

  void run(void) const;

  void setName(const std::string & name);
  const  std::string & getName(void) const;

private:
  std::string name;
};

#endif

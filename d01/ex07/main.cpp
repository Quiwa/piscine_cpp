#include <iostream>
#include <fstream>
#include <sstream>


void errorMsg(std::string msg);
std::string getFilename(std::string fileName);
std::string str_upper(std::string str);
std::string str_replace(std::string str, std::string to_find, std::string to_replace);

int main(int ac, char **av){
  std::ofstream ofs;
  std::string word;
  std::string line;

  if (ac != 4)
    errorMsg("Usage: ./replace file_name s1 s2");
  std::ifstream ifs(av[1]);
  if (!ifs.good())
    errorMsg("Invalid file");
  if (std::ifstream(getFilename(av[1])))
    errorMsg(".replace file already exists");
  try{ ofs.open(getFilename(av[1]), std::ofstream::out);}
  catch (std::exception e){std::cout << "Couldn't create file " << getFilename(av[1]) << std::endl;}
  while (std::getline(ifs, line))
    ofs << str_replace(line, av[2], av[3]) << std::endl;
}

void errorMsg(std::string msg){
    std::cerr << msg << std::endl;
    exit(0);
}

std::string getFilename(std::string fileName){
  std::stringstream str;

  str << str_upper(fileName) << ".replace";
  return (str.str());
}

std::string str_upper(std::string str)
{
  std::stringstream ret;

	for (int i = 0; str[i]; i++)
		ret << (char)toupper(str[i]);
  return (ret.str());
}

std::string str_replace(std::string str, std::string to_find, std::string to_replace){
  int idx_word = 0;
  int to_find_size = to_find.size();
  std::stringstream ret;

  if (to_find_size == 0)
    return (str);
  while (idx_word >= 0)
  {
    str = str.substr(idx_word);
    idx_word = str.find(to_find);
    if (idx_word == -1)
    {
      ret << str.substr(0);
      break ;
    }
    ret << str.substr(0, idx_word);
    ret << to_replace;
    idx_word += to_find_size;
  }
  return (ret.str());
}

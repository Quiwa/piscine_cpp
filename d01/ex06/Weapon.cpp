#include "Weapon.hpp"
#include <string>
#include <iostream>

Weapon::Weapon(std::string type){
  this->setType(type);
}

const std::string & Weapon::getType(void) const{
  return (this->_type);
}

void Weapon::setType(std::string const & type){
  this->_type = type;
}

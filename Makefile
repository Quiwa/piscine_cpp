# ************************************************************************** #
#                                                                            #
#                                                        ::::::::            #
#   Makefile                                           :+:    :+:            #
#                                                     +:+                    #
#   By: jlehideu <jlehideu@student.codam.nl>         +#+                     #
#                                                   +#+                      #
#   Created: 2018/08/02 16:58:52 by jlehideu      #+#    #+#                 #
#   Updated: 2020/11/03 11:59:52 by jlehideu      ########   odam.nl         #
#                                                                            #
# ************************************************************************** #

NAME = 

SRC = 

OBJDIR = obj/

SRCDIR = 

OBJ = $(SRC:%.cpp=$(OBJDIR)%.o)

CC = clang++

FLAGS =  -Wall -Wextra -Werror

LIB = 

INCLUDES = 

all : $(NAME)

$(NAME): $(OBJ)
	@clear
	@echo "\033[92mCreated $(NAME) obj\033[0m"
	@$(CC) $(FLAGS) $(LIB) -o $(NAME) $^
	@echo "\033[92mCompiled $(NAME)\033[0m"

$(OBJDIR)%.o: $(SRCDIR)%.cpp
	@mkdir -p obj
	@$(CC) $(INCLUDES) $(FLAGS) -c $< -o $@

clean:
	@rm -f $(OBJ)
	@rm -rf $(OBJDIR)
	@tput setaf 1
	@echo "Cleaned $(NAME) obj"
	@tput sgr0

fclean: clean
	@rm -rf $(NAME)
	@tput setaf 1
	@echo "Cleaned $(NAME) .a"
	@tput sgr0

re: fclean all

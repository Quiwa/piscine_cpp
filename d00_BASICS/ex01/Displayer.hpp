#ifndef DISPLAYER_H
# define DISPLAYER_H

#include <string>
#include "Contact.hpp"

class Displayer{
public:
  static void showTable(std::string str[4]);

private:
  static std::string truncateStr(std::string str);
  static void printStr(std::string str, int i);

};
#endif

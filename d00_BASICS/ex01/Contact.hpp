#ifndef CONTACT_H
# define CONTACT_H

#include <string>

class Contact{
	public:

		static Contact contactTable[8];
		static int contactNb;
		static void addContact(void);
		static int searchContact(void);

		std::string getFName(void) const;
		std::string getLName(void) const;
		std::string getNickname(void) const;
		std::string getLogin(void) const;
		std::string getPostalAddress(void) const;
		std::string getEmail(void) const;
		std::string getPhoneNb(void) const;
		std::string getBirthDate(void) const;
		std::string getMeal(void) const;
		std::string getColor(void) const;
		std::string getSecret(void) const;

		void setFName(void);
		void setLName(void);
		void setNickname(void);
		void setLogin(void);
		void setPostalAddress(void);
		void setEmail(void);
		void setPhoneNb(void);
		void setBirthDate(void);
		void setMeal(void);
		void setColor(void);
		void setSecret(void);


	private:

		std::string _fName;
		std::string _lName;
		std::string _nickname;
		std::string _login;
		std::string _postalAddress;
		std::string _email;
		std::string _phoneNb;
		std::string _birthDate;
		std::string _meal;
		std::string _color;
		std::string _secret;

		static void displayContactBook(Contact contact, int idx);
		static int getContactIdx();
		static void displayContactWithIdx(int contactIdx);
};

#endif

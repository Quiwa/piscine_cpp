#include <iostream>
#include <string>
#include <iomanip>
#include "Contact.hpp"
#include "Displayer.hpp"

void Displayer::showTable(std::string str[4])
{
  for (size_t i = 0; i < 4; i++) {
    str[i] = Displayer::truncateStr(str[i]);
    Displayer::printStr(str[i], i);
  }
}

std::string Displayer::truncateStr(std::string str){
  if (str.size() <= 10)
    return (str);
  str = str.substr(0, 9);
  return (str += '.');
}

void Displayer::printStr(std::string str, int i){
  std::cout << std::setw(10);
  std::cout << str;
  if (i == 3)
    std::cout << std::endl;
  else
    std::cout << "|";
}

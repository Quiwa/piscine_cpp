#include "Contact.hpp"
#include "Displayer.hpp"
#include <iostream>
#include <stdlib.h>

int Contact::contactNb = 0;
Contact Contact::contactTable[8] = {};

void Contact::addContact(void){
	Contact contact;

	std::cout << " << ADD CONTACT >>" << std::endl;
	if (Contact::contactNb == 8)
	{
		std::cout << "Max number of contacts" <<std::endl;
		return ;
	}
	std::cout << std::endl << "First name : ";
	contact.setFName();
	std::cout << std::endl << "Last name : ";
	contact.setLName();
	std::cout << std::endl << "Nickname : ";
	contact.setNickname();
	std::cout << std::endl << "Login : ";
	contact.setLogin();
	std::cout << std::endl << "Postal address : ";
	contact.setPostalAddress();
	std::cout << std::endl << "Email address : ";
	contact.setEmail();
	std::cout << std::endl << "Phone nb: ";
	contact.setPhoneNb();
	std::cout << std::endl << "Birth Date : ";
	contact.setBirthDate();
	std::cout << std::endl << "Favourite meal : ";
	contact.setMeal();
	std::cout << std::endl << "Color of underwear : ";
	contact.setColor();
	std::cout << std::endl << "Darkest secret : ";
	contact.setSecret();

	contactTable[contactNb] = contact;
	contactNb++;
}

int Contact::searchContact(void){
	int contactIdx;

	contactIdx = Contact::getContactIdx();
	if (contactIdx == -1)
		return (-1);
	Contact::displayContactWithIdx(contactIdx);
	return (0);
}

int Contact::getContactIdx()
{
	int contactIdx = -1;
	std::string userEntry;
	bool isFirstLoop = true;

	if (Contact::contactNb == 0)
	{
		std::cout << "You don't have any contact yet" << std::endl << std::endl;
		return -1;
	}
	while (contactIdx == -1)
	{
		std::cout << "<< SEARCH >>" << std::endl;
		if (!isFirstLoop)
			std::cout << "Invalid entry " << std::endl;
		std::cout << std::endl << std::endl;
		for (int i = 0; i < Contact::contactNb; i++) {
			Contact::displayContactBook(Contact::contactTable[i], i);
		}
		std::cout << std::endl << std::endl;
		std::cout << "Enter contact idx you want to see" << std::endl;
		std::cin >> userEntry;
		try {contactIdx = std::stoi(userEntry);}
		catch (...) {}
		if (contactIdx >= contactNb)
		{
			contactIdx = -1;
			std::cout << "Invalid entry" << std::endl;
		}
		std::cout << std::endl;
		isFirstLoop = false;
	}
	return (contactIdx);
}

void Contact::displayContactBook(Contact contact, int idx){
		std::string contactInfo[4];

		contactInfo[0] = std::to_string(idx);
		contactInfo[1] = contact.getFName();
		contactInfo[2] = contact.getLName();
		contactInfo[3] = contact.getLogin();
		Displayer::showTable(contactInfo);
}

void Contact::displayContactWithIdx(int contactIdx){
	std::cout << std::endl << std::endl;
	std::cout << "First name : " << Contact::contactTable[contactIdx].getFName() << std::endl;
	std::cout << "Last name : " << Contact::contactTable[contactIdx].getLName() << std::endl;
	std::cout << "Nickname : " << Contact::contactTable[contactIdx].getNickname() << std::endl;
	std::cout << "Login : " << Contact::contactTable[contactIdx].getLogin() << std::endl;
	std::cout << "Phone nb : " << Contact::contactTable[contactIdx].getPhoneNb() << std::endl;
	std::cout << "Birth date : " << Contact::contactTable[contactIdx].getBirthDate() << std::endl;
	std::cout << "Favourite meal : " << Contact::contactTable[contactIdx].getMeal() << std::endl;
	std::cout << "Color of underwear : " << Contact::contactTable[contactIdx].getColor() << std::endl;
	std::cout << "Darkest secret : " << Contact::contactTable[contactIdx].getSecret() << std::endl;
	std::cout << std::endl << std::endl;
}

void Contact::setFName(){
	std::getline(std::cin, this->_fName);
}

void Contact::setLName(){
	std::getline(std::cin, this->_lName);
}

void Contact::setNickname(){
	std::getline(std::cin, this->_nickname);
}

void Contact::setLogin(){
	std::getline(std::cin, this->_login);
}

void Contact::setPostalAddress(){
	std::getline(std::cin, this->_postalAddress);
}

void Contact::setEmail(){
	std::getline(std::cin, this->_email);
}

void Contact::setPhoneNb(){
	std::getline(std::cin, this->_phoneNb);
}

void Contact::setBirthDate(){
	std::getline(std::cin, this->_birthDate);
}

void Contact::setMeal(){
	std::getline(std::cin, this->_meal);
}

void Contact::setColor(){
	std::getline(std::cin, this->_color);
}

void Contact::setSecret(){
	std::getline(std::cin, this->_secret);
}

std::string Contact::getFName(void) const{
	return (this->_fName);
}

std::string Contact::getLName(void) const{
	return (this->_lName);
}

std::string Contact::getNickname(void) const{
	return (this->_nickname);
}

std::string Contact::getLogin(void) const{
	return (this->_login);
}

std::string Contact::getPostalAddress(void) const{
	return (this->_postalAddress);
}

std::string Contact::getEmail(void) const{
	return (this->_email);
}

std::string Contact::getPhoneNb(void) const{
	return (this->_phoneNb);
}

std::string Contact::getBirthDate(void) const{
	return (this->_birthDate);
}

std::string Contact::getMeal(void) const{
	return (this->_meal);
}

std::string Contact::getColor(void) const{
	return (this->_color);
}

std::string Contact::getSecret(void) const{
return (this->_secret);
}

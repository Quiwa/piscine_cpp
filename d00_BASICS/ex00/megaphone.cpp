#include <iostream>
#include <cctype>
#include <stdlib.h>

void print_msg_and_exit(const char *msg);
void print_str_upper(const char *str);

int main(int ac, char **av)
{
	int i = 0;

	if (ac == 1)
		print_msg_and_exit((const char *)"* LOUD AND UNBEARABLE FEEDBACK NOISE *");
	while (++i < ac)
		print_str_upper(av[i]);
	std::cout << std::endl;
	return (0);
}

void print_msg_and_exit(const char *msg)
{
	std::cout << msg << std::endl;
	exit(0);
}

void print_str_upper(const char *str)
{
	for (int i = 0; str[i]; i++)
		std::cout << (char)toupper(str[i]);
}

#include "Fixed.hpp"
#include <iostream>
#include <string>

Fixed::Fixed(void) : _fixedPoint(0){
	std::cout << "Defaut constructor called" << std::endl;
	return;
}

Fixed::Fixed(Fixed const & src){
	std::cout << "Copy constructor called" << std::endl;
	*this = src;
	return;
}

Fixed &	Fixed::operator=(Fixed const & rhs){
	std::cout << "Assignation operator called" << std::endl;
	this->_fixedPoint = rhs.getRawBits();
	return *this;
}

Fixed::~Fixed(void){
	std::cout << "Destructor called" << std::endl;
	return;
}

int Fixed::getRawBits(void) const{
	std::cout << "getRawBits member function called" << std::endl;
	return this->_fixedPoint;
}

void Fixed::setRawBits(int nb){
	std::cout << "setRawBits member function called" << std::endl;
	this->_fixedPoint = nb;
}

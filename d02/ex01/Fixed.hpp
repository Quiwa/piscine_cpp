#ifndef FIXED_H
# define FIXED_H

#include <iostream>
#include <string>

class Fixed{

public:
	Fixed(const int nb);
	Fixed(const float nb);

	float toFloat(void) const;
	int toInt(void) const;

	/*
	** CANONICAL FUNCS
	*/
	Fixed(void);
	Fixed(Fixed const & src);
	~Fixed(void);
	Fixed & operator=(Fixed const & rhs);

	/*
	** ACCESSORS
	*/
  void setRawBits(int nb);
  int getRawBits(void) const;

private:
  int _fixedPointVal;
  static const int _fractionalBits = 8;

};

std::ostream & operator<<(std::ostream & o, Fixed const & rhs);

#endif

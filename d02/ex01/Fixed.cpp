#include "Fixed.hpp"
#include <iostream>
#include <math.h>
#include <string>

float Fixed::toFloat(void) const{
	return _fixedPointVal / (float)(1 << _fractionalBits);
}

int Fixed::toInt(void) const{
	return (_fixedPointVal / (1 << _fractionalBits));
}

std::ostream & operator<<(std::ostream & o, Fixed const & rhs){
	o << rhs.toFloat();
	return (o);
}

/*
** Constructors
*/
Fixed::Fixed(const int nb){
	std::cout << "Int constructor called" << std::endl;
	this->_fixedPointVal = nb << Fixed::_fractionalBits;
}

Fixed::Fixed(const float nb){
	std::cout << "Float constructor called" << std::endl;
	this->_fixedPointVal = roundf(nb * (1 << Fixed::_fractionalBits));
}

/*
** CANONICAL FUNCS
*/
Fixed::Fixed(void) : _fixedPointVal(0){
	std::cout << "Defaut constructor called" << std::endl;
	return;
}

Fixed::Fixed(Fixed const & src){
	std::cout << "Copy constructor called" << std::endl;
	*this = src;
	return;
}

Fixed &	Fixed::operator=(Fixed const & rhs){
	std::cout << "Assignation operator called" << std::endl;
	this->_fixedPointVal = rhs._fixedPointVal;
	return *this;
}

Fixed::~Fixed(void){
	std::cout << "Destructor called" << std::endl;
	return;
}

/*
** ACCESSORS
*/
int Fixed::getRawBits(void) const{
	std::cout << "getRawBits member function called" << std::endl;
	return this->_fixedPointVal;
}

void Fixed::setRawBits(int nb){
	std::cout << "setRawBits member function called" << std::endl;
	this->_fixedPointVal = nb;
}

#ifndef A_H
# define A_H

#include <iostream>
#include <string>
#include "Base.hpp"

class A : public Base{

public:

	/*
	** CANONICAL FUNCS
	*/
	A(void);
	A(A const & src);
	~A(void);
	A & operator=(A const & rhs);

private:

};

#endif

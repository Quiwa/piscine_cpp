#include "B.hpp"
#include <iostream>
#include <string>


/*
** CANONICAL FUNCS
*/
B::B(void){
	return;
}

B::B(B const & src){
	*this = src;
	return;
}

B &	B::operator=(B const & rhs){
  (void)rhs;
	return *this;
}

B::~B(void){
	return;
}

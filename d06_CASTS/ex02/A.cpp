#include "A.hpp"
#include <iostream>
#include <string>


/*
** CANONICAL FUNCS
*/
A::A(void){
	return;
}

A::A(A const & src){
	*this = src;
	return;
}

A &	A::operator=(A const & rhs){
  (void)rhs;
	return *this;
}

A::~A(void){
	return;
}

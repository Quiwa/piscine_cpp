#include <string>
#include <iostream>
#include "data.hpp"

void * serialize(void);
struct Data deserialize(void * datas);

int main(void){
  void *serializedArray;
  struct Data data;

  serializedArray = serialize();
  data = deserialize(serializedArray);
  std::cout << "deserialize s1 " << data.s1 << std::endl;
  std::cout << "deserialize int " << data.n << std::endl;
  std::cout << "deserialize s2 " << data.s2 << std::endl;
}

void * serialize(void){
  char str[63] = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
  std::string *data = new std::string();
  int randNb;
  void * ret;

  for (size_t i = 0; i < 8; i++) {
    std::srand(time(0) * i + i % time(NULL));
    data->push_back(static_cast<char>(str[rand() % 63]));
  }
  randNb = std::rand() % 100;
  data->push_back(randNb);
  std::cout << "nb originel " << randNb << std::endl;

  for (size_t i = 0; i <  8; i++) {
    std::srand(time(0) * i + i % time(NULL));
    data->push_back(str[63 - rand() % 63]);
  }
  std::cout << "data : " << *data << std::endl;
  std::cout << "sizeof data : " << data->size() << std::endl;
  ret = reinterpret_cast<void*>(data);

  return (ret);
}

Data deserialize(void * input){
  Data ret;

  std::string *convertString = reinterpret_cast<std::string*>(input);
  char *convertNb = reinterpret_cast<char*>(input);

	ret.s1 = convertString->substr(0, 8);
  ret.n = convertNb[9];
	ret.s2 = convertString->substr(9, 20);

	return ret;
}

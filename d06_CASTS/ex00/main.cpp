#include <string>
#include <iostream>
#include "Converter.hpp"


void exitWithErrorMessage(std::string msg);

int main(int ac, char **av){

  if (ac == 1)
    exitWithErrorMessage("usage : ./convert to_convert ");
  for (int i = 1; i < ac; i++) {
    Converter converter(av[i]);
  }
  return (-1);
}

void exitWithErrorMessage(std::string msg){
    std::cerr << msg << std::endl;
    exit(0);
}

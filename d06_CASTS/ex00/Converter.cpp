#include "Converter.hpp"
#include <iostream>
#include <sstream>
#include <string>
#include <limits>
#include <float.h>
#include <cmath>


Converter::Converter(char *str)
{
  long double test;
  if (str == NULL || strlen(str) == 0)
    return ;
  if (!isParamChar(str))
    try{
      test = std::stod(str);
    }
    catch(std::exception e){
      std::cout << "char: impossible" << std::endl;
      std::cout << "int: impossible" << std::endl;
      std::cout << "float: impossible" << std::endl;
      std::cout << "double: impossible" << std::endl;
      return ;
    }
  if (isParamChar(str))
    toChar(str);
  else if (isParamInt(str))
    toInt(str);
  else if (isParamFloat(str))
    toFloat(str);
  else if (isParamDouble(str))
    toDouble(str);
  else
  {
    std::cout << "char: impossible" << std::endl;
    std::cout << "int: impossible" << std::endl;
    std::cout << "float: impossible" << std::endl;
    std::cout << "double: impossible" << std::endl;
    return ;
  }
}

/*
** CONVERTERS
*/

void Converter::toChar(std::string str){
  _cVal = str[0];
  _iVal = static_cast<int>(_cVal);
  _fVal = static_cast<float>(_cVal);
  _dVal = static_cast<double>(_cVal);
  displayChar();
  displayInt();
  displayFloat();
  displayDouble();
}

void Converter::toInt(std::string str){
    try {_iVal = std::stoi(str);}
    catch (std::exception e){}
    _cVal = static_cast<int>(_iVal);
    _fVal = static_cast<float>(_iVal);
    _dVal = static_cast<double>(_iVal);
    displayChar(_iVal > CHAR_MAX || _iVal < CHAR_MIN);
    displayInt();
    displayFloat();
    displayDouble();
}

void Converter::toFloat(std::string str){
  try {
    _fVal = std::stof(str);
  }
  catch (std::exception e){}
  _iVal = static_cast<int>(_fVal);
  _cVal = static_cast<char>(_fVal);
  _dVal = static_cast<double>(_fVal);
  displayChar(isFloatSpecialVal(str) || _fVal > CHAR_MAX || _fVal < CHAR_MIN);
  displayInt(isFloatSpecialVal(str));
  displayFloat(isFloatSpecialVal(str));
  displayDouble(isFloatSpecialVal(str));
}

void Converter::toDouble(std::string str){
  try{_dVal = std::stod(str);}
  catch (std::exception e){}
  _iVal = static_cast<int>(_dVal);
  _fVal = static_cast<float>(_dVal);
  _cVal = static_cast<char>(_dVal);
  displayChar(isDoubleSpecialVal(str) || _fVal > CHAR_MAX || _fVal < CHAR_MIN);
  displayInt(isDoubleSpecialVal(str));
  displayFloat(isDoubleSpecialVal(str));
  displayDouble(isDoubleSpecialVal(str));
}

void Converter::displayChar(bool isImpossible) const{
  if (isImpossible)
    std::cout << "char: impossible" << std::endl;
  else if (_cVal <= 31)
    std::cout << "char: Non displayable" << std::endl;
  else
    std::cout << "char: " << _cVal << std::endl;
}

void Converter::displayInt(bool isSpec) const{
	if (isSpec)
  		std::cout << "int: impossible" << std::endl;
	else
  		std::cout << "int: " << _iVal << std::endl;
}

void Converter::displayDouble(bool isSpec) const{
  std::ostringstream ost;
  ost << _dVal;
  std::cout << "double: " << ost.str();
	if (isSpec || hasSignificantNb(ost.str()))
	  std::cout << std::endl;
  else
	  std::cout << ".0" << std::endl;
}

void Converter::displayFloat(bool isSpec) const{
  std::ostringstream ost;
  ost << _fVal;
  std::cout << "float: " << ost.str();
  if (isSpec || hasSignificantNb(ost.str()))
	  std::cout << "f" << std::endl;
  else
	  std::cout << ".0f" << std::endl;
}

bool Converter::hasSignificantNb(std::string str) const{
  int i = -1;
  int size = str.size();

  while (++i < size)
    if (str[i] == '.')
      break;
  return (i != size);
}

/*
** BOOLS
*/

bool Converter::isParamChar(std::string msg) const{
  return (msg.size() == 1 && !isdigit(msg[0]));
}

bool Converter::isParamInt(std::string msg) const{
  for (size_t i = (msg[0] == '-' || msg[0] == '+') ? 1 : 0; i < msg.size(); i++) {
    if (!isdigit(msg[i]))
      return false;
  }
  return true;
}

bool Converter::isParamFloat(std::string msg) const{
  if (isDoubleSpecialVal(msg))
    return (false);
  if (isFloatSpecialVal(msg))
    return (true);
  if (msg[msg.size() - 1] != 'f')
    return (false);
  return (isParamDouble(msg));
}

bool Converter::isParamDouble(std::string msg) const{
  unsigned int dotCount = 0;

  if (isDoubleSpecialVal(msg))
    return (true);
  for (size_t i =(msg[0] == '-' || msg[0] == '+') ? 1 :  0; i < msg.size() - 1; i++)
    if (isdigit(msg[i]))
      continue ;
    else if (msg[i] == '.' && dotCount == 0)
      dotCount++;
    else
      return (false);
  return (true);
}

bool Converter::isFloatSpecialVal(std::string str) const{
  return (!std::strcmp(str.c_str(), "-inff") || !std::strcmp(str.c_str(), "- inff")  || !std::strcmp(str.c_str(), "inff") || !std::strcmp(str.c_str(), "+ inff") || !std::strcmp(str.c_str(), "+inff") || !std::strcmp(str.c_str(), "nanf"));
}

bool Converter::isDoubleSpecialVal(std::string str) const{
  return (!std::strcmp(str.c_str(), "-inf") || !std::strcmp(str.c_str(), "- inf")  || !std::strcmp(str.c_str(), "inf") || !std::strcmp(str.c_str(), "+ inf") || !std::strcmp(str.c_str(), "+inf") || !std::strcmp(str.c_str(), "nan"));
}

/*
** CANONICAL FUNCS
*/
Converter::Converter(void){
	return;
}

Converter::Converter(Converter const & src){
	*this = src;
	return;
}

Converter &	Converter::operator=(Converter const & rhs){
  this->_iVal = rhs._iVal;
  this->_fVal = rhs._fVal;
  this->_dVal = rhs._dVal;
  this->_cVal = rhs._cVal;
	return *this;
}

Converter::~Converter(void){
	return;
}

#include <iostream>
#include <string>
#include <algorithm>
#include <list>
#include <vector>
#include "easyfind.hpp"

void displayInt(int nb){
  std::cout << nb << std::endl;
}

int main(){
  std::list<int> lst;
  lst.push_back(10);
  lst.push_back(23);
  lst.push_back(3);
  std::cout << "available values" << std::endl;
  for_each(lst.begin(), lst.end(), displayInt);
  std::cout << "looking for 10 : " << std::endl;
  if (easyfind(lst, 10) != lst.end())
    std::cout << "match found " << std::endl;
  else
    std::cout << "no match found " << std::endl;
  std::cout << "looking for 11 : " << std::endl;
  if (easyfind(lst, 11) != lst.end())
    std::cout << "match found " << std::endl;
  else
    std::cout << "no match found " << std::endl;

  std::cout << "-- vectors --" << std::endl;
  std::vector<int> vec(3);
  vec[0] = 10;
  vec[1] = 23;
  vec[2] = 3;
  std::cout << "available values" << std::endl;
  for_each(vec.begin(), vec.end(), displayInt);
  std::cout << "looking for 10 : " << std::endl;
  if (easyfind(lst, 10) != lst.end())
    std::cout << "match found " << std::endl;
  else
    std::cout << "no match found " << std::endl;
  std::cout << "looking for 11 : " << std::endl;
  if (easyfind(lst, 11) != lst.end())
    std::cout << "match found " << std::endl;
  else
    std::cout << "no match found " << std::endl;

  return (0);
}

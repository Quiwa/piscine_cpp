#ifndef MUTANTSTACK_H
# define MUTANTSTACK_H

#include <iostream>
#include <list>
#include <stack>


template <typename T>
class MutantStack : public std::stack<T>{

public:
  typedef typename std::list<T>::iterator iterator;
  typedef typename std::list<T>::const_iterator const_iterator;
  typedef typename std::list<T>::const_reverse_iterator const_reverse_iterator;
  typedef typename std::list<T>::reverse_iterator reverse_iterator;

  iterator begin(void){
    return (_lst.begin());
  }

  reverse_iterator rbegin(void){
    return (_lst.rbegin());
  }

  iterator end(void){
    return (_lst.end());
  }

  reverse_iterator rend(void){
    return (_lst.rend());
  }

  //const iterator et reverse iteraor

  /*
  ** Element access
  */
  T & top(void){
    if (!_lst.empty())
      return (*_lst.begin());
    throw std::out_of_range("");
  }

  const T & top(void) const{
    if (!_lst.empty())
      return (*_lst.begin());
    throw std::out_of_range("");
  }

  /*
  ** Capacity
  */
  bool empty(void) const{
    return (_lst.empty());
  }

  size_t size(void) const{
    return (_lst.size());
  }

  /*
  ** Modifiers
  */
  void push(T elem){
    _lst.push_front(elem);
  }

  void pop(void){
    if (!empty())
      _lst.erase(_lst.begin());
  }

  void swap(MutantStack & other){
    std::list<T> tmpList = _lst;
    _lst = other._lst;
    other._lst = tmpList;
  }

	/*
	** CANONICAL FUNCS
	*/
	MutantStack(void){
  }

	MutantStack(MutantStack const & src){
	   *this = src;
  }

	virtual ~MutantStack(void){
  }

	MutantStack & operator=(MutantStack const & rhs){
    _lst = rhs._lst;
  	return *this;
  }

private:
  std::list<T> _lst;

};

#endif

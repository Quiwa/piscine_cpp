#ifndef SPAN_H
# define SPAN_H

#include <iostream>
#include <string>
#include <vector>

class span{

public:
	span(unsigned int nb);
	void addNumber(int nb);
  int shortestSpan(void);
  int longestSpan(void);

	/*
	** CANONICAL FUNCS
	*/
	span(span const & src);
	~span(void);
	span & operator=(span const & rhs);

private:
  unsigned int _size;
  unsigned int _idx;
  std::vector<int> _vec;

	/*
	** CANONICAL FUNCS
	*/
	span(void);
};

#endif

#include "span.hpp"
#include <iostream>
#include <string>
#include <limits>


span::span(unsigned int nb) : _size(nb), _idx(0){
	return;
}

void span::addNumber(int nb){
  if (_idx == _size)
    throw std::out_of_range("");
  _vec.push_back(nb);
	_idx++;
}

int span::longestSpan(void){
	if (_idx < 2)
		throw ("span is too short");
  return (abs(*std::max_element(_vec.begin(), _vec.end()) - *std::min_element(_vec.begin(), _vec.end())));
}

int span::shortestSpan(void){
  long int shortest = std::numeric_limits<long>::max();

  if (_idx <= 1)
		throw ("span is too short");
  for (size_t i = 0; i < _idx; i++)
  	for (size_t j = 0; j < _idx; j++)
			if (i != j && abs(_vec[j] - _vec[i]) < shortest)
				if ((shortest = abs(_vec[j] - _vec[i])) == 0)
					return (shortest);
 return (shortest);
}

/*
** CANONICAL FUNCS
*/
span::span(span const & src){
	*this = src;
	return;
}

span & span::operator=(span const & rhs){
  this->_size  = rhs._size;
  this->_idx = rhs._idx;
  this->_vec = rhs._vec;
	return *this;
}

span::~span(void){
	return;
}

#ifndef SCAVTRAP_H
# define SCAVTRAP_H

#include <iostream>
#include <string>

class ScavTrap{

public:
	ScavTrap(void);
	ScavTrap(std::string name);
	ScavTrap(ScavTrap const & src);
	~ScavTrap(void);
	ScavTrap & operator=(ScavTrap const & rhs);

  void rangedAttack(std::string const & target);
  void meleeAttack(std::string const & target);
  void takeDamage(unsigned int amount);
  void beRepaired(unsigned int amount);

	void challengeNewcomer(std::string const & target);

  std::string getName() const;
  int getHitPoints() const;
  int getMaxHitPoints() const;
  int getEnergyPoints() const;
	int getLevel() const;
  int getMaxEnergyPoints() const;
  int getRangedAttackDamage() const;
  int getMeleeAttackDamage() const;
  int getArmorDamageReduction() const;

	void challengePolitely(std::string target);
	void challengeArchly(std::string target);
	void challengeSneaky(std::string target);
	void challengeBug(std::string target);
	void challengeMadly(std::string target);


private:
  std::string _name;
  int _hitPoints;
  int _maxHitPoints;
  int _energyPoints;
  int _maxEnergyPoints;
  int _level;
  int _meleeAttackDamage;
  int _rangedAttackDamage;
  int _armorDamageReduction;

	void (ScavTrap::*_challenges[5])(std::string target);
	void setEnergyPoints(int val);
	void setHitPoints(int val);
	int getAmountOfDamages(int amount);
};

#endif

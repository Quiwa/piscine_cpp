#include "ClapTrap.hpp"
#include <iostream>
#include <string>

void ClapTrap::rangedAttack(std::string const & msg){
  std::cout << msg << std::endl;
	this->setEnergyPoints(this->_energyPoints - 25);
}

void ClapTrap::meleeAttack(std::string const & msg){
  std::cout << msg << std::endl;
	this->setEnergyPoints(this->_energyPoints - 25);
}

void ClapTrap::takeDamage(unsigned int amount){
	amount = getAmountOfDamages(amount);
	setHitPoints(this->_hitPoints - amount);
}

void ClapTrap::beRepaired(unsigned int amount){
	setHitPoints(this->_hitPoints + amount);
}

int ClapTrap::getAmountOfDamages(int amount){
	return ((amount >= _armorDamageReduction) ? amount - _armorDamageReduction : 0);
}

/*
** CANONICAL FUNCS
*/
ClapTrap::ClapTrap(void){
	return ;
}

ClapTrap::ClapTrap(std::string name, int hitPoints, int maxHitPoint, int energyPoint, int maxEnergyPoint, int meleeAttackDamage, int rangedAttackDamage, int armorDamageReduction, int level)
:
					_name(name),
					_hitPoints(hitPoints),
					_maxHitPoints(maxHitPoint),
					_energyPoints(energyPoint),
					_maxEnergyPoints(maxEnergyPoint),
					_meleeAttackDamage(meleeAttackDamage),
					_rangedAttackDamage(rangedAttackDamage)
{
	this->_armorDamageReduction = armorDamageReduction;
	this->_level = level;
	std::cout << "ClapTrap constructor called"  << std::endl;
	return;
}

ClapTrap::ClapTrap(ClapTrap const & src){
	std::cout << "Claptrap copy constructor called" << std::endl;
	*this = src;
	return;
}

ClapTrap &	ClapTrap::operator=(ClapTrap const & rhs){
	this->_name = rhs.getName();
	this->_hitPoints = rhs.getHitPoints();
	this->_maxHitPoints = rhs.getMaxHitPoints();
	this->_energyPoints = rhs.getEnergyPoints();
	this->_maxEnergyPoints = rhs.getMaxEnergyPoints();
	this->_meleeAttackDamage = rhs.getMeleeAttackDamage();
	this->_rangedAttackDamage = rhs.getRangedAttackDamage();
	this->_armorDamageReduction = rhs.getArmorDamageReduction();
	this->_level = rhs.getLevel();
	return *this;
}

ClapTrap::~ClapTrap(){
	std::cout << "ClapTrap destructor called for ClapTrap" << std::endl;
	return;
}

/*
** ACCESSORS
*/
std::string ClapTrap::getName() const{
	return (this->_name);
}

int ClapTrap::getHitPoints() const{
	return (this->_hitPoints);
}

int ClapTrap::getMaxHitPoints() const{
	return (this->_maxHitPoints);
}

int ClapTrap::getEnergyPoints() const{
	return (this->_energyPoints);
}

int ClapTrap::getMaxEnergyPoints() const{
	return (this->_maxEnergyPoints);
}

int ClapTrap::getLevel() const{
	return (this->_level);
}

int ClapTrap::getMeleeAttackDamage() const{
	return (this->_meleeAttackDamage);
}

int ClapTrap::getRangedAttackDamage() const{
	return (this->_rangedAttackDamage);
}

int ClapTrap::getArmorDamageReduction() const{
	return (this->_armorDamageReduction);
}

void ClapTrap::setEnergyPoints(int val){
	if (val < 0)
		this->_energyPoints = 0;
	else if (val > this->_maxHitPoints)
		this->_energyPoints = this->_maxEnergyPoints;
	else
		this->_energyPoints = val;
}

void ClapTrap::setHitPoints(int val){
	if (val < 0)
		this->_hitPoints= 0;
	else if (val > this->_maxHitPoints)
		this->_hitPoints = this->_maxHitPoints;
	else
		this->_hitPoints = val;
}

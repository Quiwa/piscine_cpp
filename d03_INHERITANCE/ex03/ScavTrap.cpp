#include "ScavTrap.hpp"
#include "ClapTrap.hpp"
#include <iostream>
#include <string>

ScavTrap::ScavTrap(void){
	return ;
}

ScavTrap::ScavTrap(std::string name)
:
 ClapTrap(name, 100, 100, 50, 50, 20, 15, 3, 1)
{
	std::cout << "ScavTrap constructor called" << std::endl << std::endl;
	this->_challenges[0] = &ScavTrap::challengePolitely;
	this->_challenges[1] = &ScavTrap::challengeArchly;
	this->_challenges[2] = &ScavTrap::challengeSneaky;
	this->_challenges[3] = &ScavTrap::challengeBug;
	this->_challenges[4] = &ScavTrap::challengeMadly;
	return;
}

ScavTrap::ScavTrap(ScavTrap const & src)
:
ClapTrap(src)
{
	std::cout << "ScavTrap copy constructor called" << std::endl;
	*this = src;
	return;
}

ScavTrap &	ScavTrap::operator=(ScavTrap const & rhs){
	ClapTrap::operator=(rhs);
	for (size_t i = 0; i < 5; i++)
		this->_challenges[i] = rhs._challenges[i];
	return *this;
}

void ScavTrap::challengeNewcomer(std::string const & target){
	if (this->getEnergyPoints() == 0)
	{
		std::cout << this->_name << " is out of energy... But still embody swganess." << std::endl << std::endl;
		return ;
	}
	(this->*_challenges[std::rand() % 5])(target);
	std::cout << std::endl;
}

void ScavTrap::challengePolitely(std::string target){
	std::cout << "Hi " << target << ", feel free to enter at our own risk" << std::endl;
}

void ScavTrap::challengeArchly(std::string ){
	std::cout << "Do I try to enter your devil lair ? DO I ? Get the f*ck off" << std::endl;
}

void ScavTrap::challengeSneaky(std::string target){
	std::cout << "Hi " << target << ", sure come ! * activates an awful trap behind the door *" << std::endl;
}

void ScavTrap::challengeBug(std::string ){
	std::cout << "Update 1.0.222 is now available for your SC4V-TP" << std::endl;
}

void ScavTrap::challengeMadly(std::string target){
	std::cout << "Hi " << target << ", AYAAAAAH hahahehehehehe *cough cough* ahem" << std::endl;
}

ScavTrap::~ScavTrap(){
	std::cout << "Destructor called" << std::endl;
	return;
}

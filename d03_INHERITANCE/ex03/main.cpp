#include "FragTrap.hpp"
#include "ScavTrap.hpp"
#include "ClapTrap.hpp"
#include "NinjaTrap.hpp"

int main(void){
  FragTrap ExhaustedOne("ExhaustedOne");
  ScavTrap JohnTucker("John Tucker");
  NinjaTrap NinjaDark("Dark sasuke");
  NinjaTrap NinjaLight("Light sasuke");
  NinjaTrap *NinjaCpy = new NinjaTrap(NinjaDark);
  NinjaTrap  *NinjaCpycpy = new NinjaTrap();

  *NinjaCpycpy = *NinjaCpy;
  delete(NinjaCpy);

  ExhaustedOne.vaulthunter_dot_exe("You");

  JohnTucker.challengeNewcomer("You");

  NinjaCpycpy->ninjaShoebox(ExhaustedOne);
  NinjaCpycpy->ninjaShoebox(JohnTucker);
  NinjaCpycpy->ninjaShoebox(NinjaLight);
  return (0);
}

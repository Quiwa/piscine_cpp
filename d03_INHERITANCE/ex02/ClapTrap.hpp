#ifndef CLAPTRAP_H
# define CLAPTRAP_H

#include <iostream>
#include <string>

class ClapTrap{

public:
	ClapTrap(void);
	ClapTrap(std::string name, int hitPoints, int maxHitPoint, int energyPoint, int maxEnergyPoint, int meleeAttackDamage, int rangedAttackDamage, int armorDamageReduction, int level);
	ClapTrap(ClapTrap const & src);
	~ClapTrap(void);
	ClapTrap & operator=(ClapTrap const & rhs);

  void rangedAttack(const std::string & msg);
  void meleeAttack(const std::string & msg);
  void takeDamage(unsigned int amount);
  void beRepaired(unsigned int amount);

	/*
	** ACCESSORS
	*/
  std::string getName() const;
  int getHitPoints() const;
  int getMaxHitPoints() const;
  int getEnergyPoints() const;
	int getLevel() const;
  int getMaxEnergyPoints() const;
  int getRangedAttackDamage() const;
  int getMeleeAttackDamage() const;
  int getArmorDamageReduction() const;

	void setEnergyPoints(int val);
	void setHitPoints(int val);

protected:
  std::string _name;
  int _hitPoints;
  int _maxHitPoints;
  int _energyPoints;
  int _maxEnergyPoints;
  int _level;
  int _meleeAttackDamage;
  int _rangedAttackDamage;
  int _armorDamageReduction;

	int getAmountOfDamages(int amount);
};

#endif

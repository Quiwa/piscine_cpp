#ifndef SCAVTRAP_H
# define SCAVTRAP_H

#include <iostream>
#include <string>
#include "ClapTrap.hpp"

class ScavTrap : public ClapTrap{

public:
	ScavTrap(void);
	ScavTrap(std::string name);
	ScavTrap(ScavTrap const & src);
	~ScavTrap(void);
	ScavTrap & operator=(ScavTrap const & rhs);

	void challengeNewcomer(std::string const & target);

	void challengePolitely(std::string target);
	void challengeArchly(std::string target);
	void challengeSneaky(std::string target);
	void challengeBug(std::string target);
	void challengeMadly(std::string target);



private:
	void (ScavTrap::*_challenges[5])(std::string target);

};

#endif

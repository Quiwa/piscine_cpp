#include "NinjaTrap.hpp"
#include "FragTrap.hpp"
#include "SuperTrap.hpp"
#include <iostream>
#include <string>

SuperTrap::SuperTrap(void){
	return ;
}
SuperTrap::SuperTrap(std::string name)
:
	ClapTrap(name, 100, 100, 120, 120, 60, 20, 5, 1),
	NinjaTrap(name),
	FragTrap(name)
{
	std::cout << "SuperTrap constructor called" << std::endl << std::endl;
	return;
}

SuperTrap::SuperTrap(SuperTrap const & src)
:
NinjaTrap(src),
FragTrap(src)
{
	std::cout << "SuperTrap copy constructor called" << std::endl;
	*this = src;
	return;
}

SuperTrap & SuperTrap::operator=(SuperTrap const & rhs)
{
	NinjaTrap::operator=(rhs);
	return *this;
}

SuperTrap::~SuperTrap(){
	std::cout << "SuperTrap destructor called" << std::endl;
	return;
}

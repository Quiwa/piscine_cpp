#include "FragTrap.hpp"
#include "ScavTrap.hpp"
#include "ClapTrap.hpp"
#include "NinjaTrap.hpp"
#include "SuperTrap.hpp"

int main(void){
  FragTrap ExhaustedOne("ExhaustedOne");
  std::cout << std::endl;
  ScavTrap JohnTucker("John Tucker");
  std::cout << std::endl;
  NinjaTrap NinjaDark("Dark sasuke");
  std::cout << std::endl;
  NinjaTrap NinjaLight("Light sasuke");
  std::cout << std::endl;
  SuperTrap SuperTrap("Super man");
  std::cout << std::endl;

  ExhaustedOne.vaulthunter_dot_exe("You");

  JohnTucker.challengeNewcomer("You");

  NinjaDark.ninjaShoebox(ExhaustedOne);
  NinjaDark.ninjaShoebox(JohnTucker);
  NinjaDark.ninjaShoebox(NinjaLight);

  SuperTrap.ninjaShoebox(ExhaustedOne);
  SuperTrap.ninjaShoebox(JohnTucker);
  SuperTrap.ninjaShoebox(NinjaLight);
  SuperTrap.vaulthunter_dot_exe("Coucou");
  std::cout << std::endl;
  return (0);
}

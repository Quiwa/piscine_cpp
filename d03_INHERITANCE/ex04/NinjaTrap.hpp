#ifndef NINJATRAP_H
# define NINJATRAP_H

#include <iostream>
#include <string>
#include "ClapTrap.hpp"
#include "FragTrap.hpp"
#include "ScavTrap.hpp"

class NinjaTrap : virtual public ClapTrap{

public:

	void ninjaShoebox(FragTrap & target);
	void ninjaShoebox(ScavTrap & target);
	void ninjaShoebox(NinjaTrap & target);

	NinjaTrap(std::string name);

	/*
	** CANONICAL FUNCS
	*/
	NinjaTrap(void);
	NinjaTrap(NinjaTrap const & src);
	~NinjaTrap(void);
	NinjaTrap & operator=(NinjaTrap const & rhs);
};

#endif

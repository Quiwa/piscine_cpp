#include "FragTrap.hpp"

int main(void){
  FragTrap *ExhaustedOne = new FragTrap("ExhaustedOne");
  FragTrap *cpy = new FragTrap("cpy");
  FragTrap *cpycpy = new FragTrap(*cpy);

  *cpy = *ExhaustedOne;
  delete(ExhaustedOne);
  for (size_t i = 0; i < 6; i++){
	  std::srand(std::time(0) + i);
    cpy->vaulthunter_dot_exe("You");
  }
  delete(cpy);
  for (size_t i = 0; i < 6; i++){
	  std::srand(std::time(0) + i);
    cpycpy->vaulthunter_dot_exe("You");
  }
  delete(cpycpy);
  return (0);
}

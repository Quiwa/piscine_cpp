#include "Test.hpp"
#include <iostream>
#include <string>


int Test::get( void ) const {
	return this->_n;
}

/*
** CANONICAL FUNCS
*/
Test::Test( void ) : _n( 42 ) {
	return;
}

Test::Test(Test const & src){
	*this = src;
	return;
}

Test &	Test::operator=(Test const & rhs){
	this->_n = rhs._n;
	return *this;
}

Test &	Test::operator=(int const & rhs){
	this->_n = rhs;
	return *this;
}

Test::~Test(void){
	return;
}

std::ostream & operator<<( std::ostream & o, Test const & rhs ) {
	o << rhs.get();
	return o;
}

#ifndef ARRAY_H
# define ARRAY_H

#include <iostream>
#include <string>
#include <stdexcept>

template <typename T>
class Array{

public:

	Array(unsigned int nb = 0) : _size(nb){
    _size = nb;
    _array = new T[_size];
    for (unsigned int i = 0; i < _size; i++) {
      _array[i] = 0;
    }
  }

	/*
	** CANONICAL FUNCS
	*/

	Array(Array const & src){
    this->_size = src._size;
    this->_array = new T[_size];
    for (size_t i = 0; i < _size; i++)
      this->_array[i] = src._array[i];
  }

	~Array(void){
	delete [] _array;
  }

	Array<T>& operator=(const Array<T> & rhs){
	delete [] _array;
    this->_size = rhs._size;
    this->_array = new T[_size];
    for (size_t i = 0; i < _size; i++)
      this->_array[i] = rhs._array[i];
    return (*this);
  }

	T & operator[](unsigned int nb){
    if (nb >= _size)
      throw std::out_of_range("");
    return (_array[nb]);
  }

  /*
  ** ACCESSORS
  */
  unsigned int size() const{
    return (_size);
  }

private:
  T *_array;
  unsigned int _size;

};

template <typename T>
std::ostream & operator<<(std::ostream & os, Array<T> & array){
  os << "<array> :";
  for (unsigned int i = 0; i < array.size(); i++) {
    os << "[" << i <<"] -> " << array[i] << "; ";
  }
  return (os);
}

#endif

#ifndef TEST_H
# define TEST_H

#include <iostream>
#include <string>

class Test{

public:

  int get(void) const;
	/*
	** CANONICAL FUNCS
	*/
	Test(void);
	Test(Test const & src);
	~Test(void);
	Test & operator=(Test const & rhs);

private:
  int _n;
};

std::ostream & operator<<( std::ostream & o, Test const & rhs );

#endif

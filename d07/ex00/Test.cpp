#include <iostream>
#include <string>
#include "Test.hpp"

Test::Test( int n ) : _n( n ) {
}

/*
** CANONICAL FUNCS
*/
Test::Test(void){
}

Test::Test(Test const & src){
	*this = src;
}

Test::~Test(void){
}

/*
** ACCESSORS
*/
int Test::getN(void) {
	return (_n);
}

Test & Test::operator=( Test const & rhs ) {
	 this->_n = rhs._n;
	 return (*this);
 }

bool Test::operator==( Test const & rhs ) {
	 return (this->_n == rhs._n);
 }

bool Test::operator!=( Test const & rhs ) {
	 return (this->_n != rhs._n);
 }

bool Test::operator>( Test const & rhs ) {
	 return (this->_n > rhs._n);
 }

bool Test::operator<( Test const & rhs ) {
	 return (this->_n < rhs._n);
 }

bool Test::operator>=( Test const & rhs ) {
	 return (this->_n >= rhs._n);
 }

bool Test::operator<=( Test const & rhs ) {
	 return (this->_n <= rhs._n);
 }

std::ostream & operator<<(std::ostream & os, Test & rhs){
  os << rhs.getN();
  return (os);
}

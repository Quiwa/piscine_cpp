#include <iostream>
#include <string>

#ifndef TEST_H
# define TEST_H

class Test {

	public:
		Test( int n );
		/*
		** CANONICAL FUNCS
		*/
		Test(void);
		Test(Test const & src);
		~Test(void);
		Test & operator=(Test const & rhs);

		/*
		** ACCESSORS
		*/
		int getN(void);

		/*
		** OPERATORS
		*/
		bool operator==(Test const & rhs);
		bool operator!=(Test const & rhs);
		bool operator>(Test const & rhs) ;
		bool operator<=(Test const & rhs);
		bool operator<(Test const & rhs);
		bool operator>=(Test const & rhs);

	private:

		int _n;
};

std::ostream & operator<<(std::ostream & os, Test & rhs);

#endif

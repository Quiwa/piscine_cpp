#include <iostream>
#include "Test.hpp"

template <typename T>
void swap(T & a, T & b){
	T tmp;

	tmp = a;
	a = b;
	b = tmp;
}

template <typename T>
T & min(T & a, T & b){
	return ((a < b) ? a : b);
}

template <typename T>
T & max(T & a, T & b){
	return ((a > b) ? a : b);
}

int main(void){
	int a = 2;
	int b = 3;
	::swap( a, b );
	std::cout << "a = " << a << ", b = " << b << std::endl;
	std::cout << "min( a, b ) = " << ::min( a, b ) << std::endl;
	std::cout << "max( a, b ) = " << ::max( a, b ) << std::endl;
	std::string c = "chaine1";
	std::string d = "chaine2";
	::swap(c, d);
	std::cout << "c = " << c << ", d = " << d << std::endl;
	std::cout << "min( c, d ) = " << ::min( c, d ) << std::endl;
	std::cout << "max( c, d ) = " << ::max( c, d ) << std::endl;
	char e = 'e';
	char f = 'f';
	std::cout << "e = " << e << ", f = " << f << std::endl;
	::swap(e, f);
	std::cout << "e = " << e << ", f = " << f << std::endl;
	std::cout << "min( e, f ) = " << ::min( e, f ) << std::endl;
	std::cout << "max( e, f ) = " << ::max( e, f ) << std::endl;
	Test A(1);
	Test B(2);
	std::cout << "A = " << A << ", B = " << B << std::endl;
	::swap<Test>(A, B);
	std::cout << "A = " << A << ", B = " << B << std::endl;
	std::cout << "min( A, B ) = " <<  ::min(A, B) << std::endl;
	std::cout << "max( A, B ) = " << ::max(A, B) << std::endl;

return 0;
}

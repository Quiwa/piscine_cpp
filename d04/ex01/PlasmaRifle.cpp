#include "PlasmaRifle.hpp"
#include <iostream>
#include <string>

void PlasmaRifle::attack() const{
  std::cout << "* piouuu piouuu piouuu *" << std::endl;
}

/*
** Cannonical declarations
*/
PlasmaRifle::PlasmaRifle(void):
AWeapon("Plasma Rifle", 5, 21)
{
	return;
}

PlasmaRifle::PlasmaRifle(PlasmaRifle const & src){
	*this = src;
	return;
}

PlasmaRifle &	PlasmaRifle::operator=(PlasmaRifle const & rhs){
  AWeapon::operator=(rhs);
	return *this;
}

PlasmaRifle::~PlasmaRifle(void){
	return;
}

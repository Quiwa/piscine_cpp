#include "RadScorpion.hpp"
#include "Enemy.hpp"
#include <iostream>
#include <string>

void RadScorpion::takeDamage(int damage){
  Enemy::takeDamage(damage);
}

/*
** CANONICAL FUNCTIONS
*/
RadScorpion::RadScorpion(void){
  this->setHP(80);
  this->setType("RadScorpion");
  std::cout << "* click click click *" << std::endl;
	return;
}

RadScorpion::RadScorpion(RadScorpion const & src){
  std::cout << "* click click click *" << std::endl;
	*this = src;
}

RadScorpion &	RadScorpion::operator=(RadScorpion const & rhs){
  Enemy::operator=(rhs);
	return (*this);
}

RadScorpion::~RadScorpion(void){
  std::cout << "* SPROTCH *" << std::endl;
	return;
}

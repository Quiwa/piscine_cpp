#ifndef ENEMY_H
# define ENEMY_H

#include <iostream>
#include <string>

class Enemy{

public:

  Enemy(int hp, std::string const & type);
  virtual void takeDamage(int);

  /*
  ** ACCESSORS
  */
  int getHP() const;
  std::string const & getType() const;
  void setHP(int hp);
  void setType(std::string const & type);

  /*
  ** Canonical funcs
  */
	Enemy(void);
	Enemy(Enemy const & src);
	virtual ~Enemy(void);
	Enemy & operator=(Enemy const & rhs);

private:
  int _hp;
  std::string _type;


};

#endif

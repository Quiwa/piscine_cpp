#include "Character.hpp"
#include "Enemy.hpp"
#include "AWeapon.hpp"
#include <iostream>
#include <string>

Character::Character(std::string const & name){
  this->_name = name;
  this->_AP = 40;
  this->_weapon = NULL;
}

void Character::recoverAP(void){
  setAP(_AP + 10);
}

void Character::setAP(int ap){
  this->_AP = (ap > 40) ? 40 : (ap > 0) ? ap : 0;
}

void Character::equip(AWeapon * weapon){
  this->_weapon = weapon;
}

void Character::attack(Enemy * enemy){
  if (this->_weapon == NULL || this->_AP < this->_weapon->getAPCost())
    return ;
  std::cout << this->_name << " attacks " << enemy->getType() << " with a "  << this->_weapon->getName() << std::endl;
  this->_weapon->attack();
  enemy->takeDamage(this->_weapon->getDamage());
  this->setAP(this->getAP() - this->_weapon->getAPCost());
  if (enemy->getHP() <= 0)
    delete(enemy);
}

/*
** GETTERS / SETTERS
*/
std::string const & Character::getName() const{
  return (this->_name);
}

int Character::getAP() const{
  return (this->_AP);
}

AWeapon * Character::getWeapon() const{
  return (this->_weapon);
}

AWeapon * Character::getWeaponCpy() const{
  AWeapon * weaponCpy = NULL;
  *weaponCpy = *this->_weapon;
  return (weaponCpy);
}

/*
** CANONICAL FUNC
*/
Character::Character(void){
	return;
}

Character::Character(Character const & src){
	*this = src;
	return;
}

Character &	Character::operator=(Character const & rhs){
  this->_name = rhs.getName();
  this->_AP = rhs.getAP();
  this->_weapon = rhs.getWeaponCpy();
	return *this;
}

Character::~Character(void){
	return;
}

std::ostream & operator<<(std::ostream & os, Character & character){
  if (character.getWeapon() == NULL)
    os << character.getName() << " has " << character.getAP() << " AP and is unarmed" << std::endl;
  else
    os << character.getName() << " has " << character.getAP() << " AP and wields a " << character.getWeapon()->getName() << std::endl;
  return (os);
}

#ifndef KNIFE_H
# define KNIFE_H

#include <iostream>
#include <string>
#include "AWeapon.hpp"

class Knife : public AWeapon{

public:
  virtual void attack(void) const;

	/*
	** CANONICAL FUNCS
	*/
	Knife(void);
	Knife(Knife const & src);
	virtual ~Knife(void);
	Knife & operator=(Knife const & rhs);

private:

};

#endif

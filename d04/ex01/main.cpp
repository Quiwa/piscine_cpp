#include "AWeapon.hpp"
#include "PlasmaRifle.hpp"
#include "PowerFist.hpp"
#include "Knife.hpp"
#include "Enemy.hpp"
#include "SuperMutant.hpp"
#include "SuperVilain.hpp"
#include "RadScorpion.hpp"
#include "Character.hpp"

int main()
{
  Character* me = new Character("me");
  std::cout << *me;
  Enemy* b = new RadScorpion();
  Enemy* c = new SuperMutant();
  Enemy* d = new SuperVilain();
  AWeapon* pr = new PlasmaRifle();
  AWeapon* pf = new PowerFist();
  AWeapon* knife = new Knife();
  me->equip(pr);
  std::cout << *me;
  me->equip(pf);
  me->attack(b);
  std::cout << *me;
  me->equip(pr);
  std::cout << b->getHP() << std::endl;
  std::cout << c->getHP() << std::endl;
  std::cout << *me;
  me->attack(b);
  std::cout << *me;
  me->attack(c);
  std::cout << *me;
  me->attack(c);
  std::cout << *me;
  me->attack(b);
  std::cout << *me;
  std::cout << b->getHP() << std::endl;
  std::cout << c->getHP() << std::endl;
  me->attack(c);
  std::cout << *me;
  me->attack(c);
  std::cout << *me;
  me->attack(b);
  me->equip(pr);
  me->attack(b);
  me->attack(d);
  me->equip(knife);
  std::cout << c->getHP() << std::endl;
  std::cout << *me;
  return 0;
}

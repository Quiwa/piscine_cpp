#include "PowerFist.hpp"
#include <iostream>
#include <string>

void PowerFist::attack() const{
  std::cout << "* pschhh... SBAM! *" << std::endl;
}

/*
** Cannonical declarations
*/
PowerFist::PowerFist(void):
AWeapon("Power Fist", 8, 50)
{
	return;
}

PowerFist::PowerFist(PowerFist const & src){
	*this = src;
	return;
}

PowerFist &	PowerFist::operator=(PowerFist const & rhs){
  AWeapon::operator=(rhs);
	return *this;
}

PowerFist::~PowerFist(void){
	return;
}

#include "Knife.hpp"
#include <iostream>
#include <string>


void Knife::attack() const{
  std::cout << "* scouick scouick *" << std::endl;
}

/*
** CANONICAL FUNCS
*/
Knife::Knife(void):
AWeapon("Knife", 2, 30)
{
	return;
}

Knife::Knife(Knife const & src){
	*this = src;
	return;
}

Knife &	Knife::operator=(Knife const & rhs){
  AWeapon::operator=(rhs);
	return *this;
}

Knife::~Knife(void){
	return;
}

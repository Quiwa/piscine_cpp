#include "AWeapon.hpp"
#include <iostream>
#include <string>

AWeapon::AWeapon(void){
	return;
}

AWeapon::AWeapon(std::string const & name, int apcost, int damage){
  this->_name = name;
  this->_APCost = apcost;
  this->_damage = damage;
}

AWeapon::AWeapon(AWeapon const & src){
	*this = src;
	return;
}

AWeapon &	AWeapon::operator=(AWeapon const & rhs){
  this->_name = rhs._name;
  this->_damage = rhs._damage;
  this->_APCost = rhs._APCost;
  this->_lightningEffect = rhs._lightningEffect;
  this->_sound = rhs._sound;
	return *this;
}

AWeapon::~AWeapon(void){
	return;
}

std::string const & AWeapon::getName(void) const{
  return (this->_name);
}

int AWeapon::getAPCost(void) const{
  return (this->_APCost);
}

int AWeapon::getDamage(void) const{
  return (this->_damage);
}

#include "SuperVilain.hpp"
#include "Enemy.hpp"
#include <iostream>
#include <string>


void SuperVilain::takeDamage(int damage){
  Enemy::takeDamage(damage);
}

/*
** CANONICAL FUNCS
*/
SuperVilain::SuperVilain(void){
  this->setHP(200);
  this->setType("Super Vilain");
  std::cout << "I'm the baaad guy" << std::endl;
	return;
}

SuperVilain::SuperVilain(SuperVilain const & src){
	*this = src;
	return;
}

SuperVilain &	SuperVilain::operator=(SuperVilain const & rhs){
  Enemy::operator=(rhs);
	return *this;
}

SuperVilain::~SuperVilain(void){
  std::cout << "NOOooooooOooo..." << std::endl;
	return;
}

#ifndef POWER_FIST_H
# define POWER_FIST_H

#include <iostream>
#include <string>
#include "AWeapon.hpp"

class PowerFist : public AWeapon{

public:
  virtual void attack(void) const;

  /*
  ** Abstract class funcs
  */
	PowerFist(void);
	PowerFist(PowerFist const & src);
	virtual ~PowerFist(void);
	PowerFist & operator=(PowerFist const & rhs);

private:

};

#endif

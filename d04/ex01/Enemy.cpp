#include "Enemy.hpp"
#include <iostream>
#include <string>

void Enemy::takeDamage(int damage){
	if (damage < 0)
		return ;
	this->setHP(this->_hp - damage);
}

/*
** getter / setters
*/

std::string const & Enemy::getType() const{
	return (this->_type);
}

int Enemy::getHP() const{
	return (this->_hp);
}

void Enemy::setHP(int hp){
	if (hp < 0)
		this->_hp = 0;
	else
		this->_hp = hp;
}

void Enemy::setType(std::string const & type){
	this->_type = type;
}

/*
** Abstract class funcs
*/
Enemy::Enemy(void){
	return;
}

Enemy::Enemy(Enemy const & src){
	*this = src;
	return;
}

Enemy &	Enemy::operator=(Enemy const & rhs){
	this->_hp = rhs.getHP();
	this->_type = rhs.getType();
	return *this;
}

Enemy::~Enemy(void){
	return;
}

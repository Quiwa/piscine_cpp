#ifndef SUPERMUTANT_H
# define SUPERMUTANT_H

#include <iostream>
#include <string>
#include "Enemy.hpp"

class SuperMutant : public Enemy{

public:
  virtual void takeDamage(int damage);

  /*
  ** CANONICAL FUNC
  */
	SuperMutant(void);
	SuperMutant(SuperMutant const & src);
	virtual ~SuperMutant(void);
	SuperMutant & operator=(SuperMutant const & rhs);

private:
};

#endif

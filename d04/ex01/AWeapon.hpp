#ifndef AWEAPON_H
# define AWEAPON_H

#include <iostream>
#include <string>

class AWeapon{

public:
  AWeapon(std::string const & name, int apcost, int damage);
  virtual void attack() const = 0;
  
  /*
  ** ACCESSORS
  */
  std::string const & getName() const;
  int getAPCost() const;
  int getDamage() const;

  /*
  ** CANONICAL FUNCS
  */
	AWeapon(void);
	AWeapon(AWeapon const & src);
	virtual ~AWeapon(void);
	AWeapon & operator=(AWeapon const & rhs);

private:
  std::string _name;
  int _damage;
  int _APCost;
  std::string _lightningEffect;
  std::string _sound;
};

#endif

#ifndef CHARACTER_H
# define CHARACTER_H

#include <string>
#include "AMateria.hpp"
#include "ICharacter.hpp"

class Character : public ICharacter
{
  public:
    virtual void equip(AMateria* m);
    virtual void unequip(int idx);
    virtual void use(int idx, ICharacter& target);

    Character(std::string const & name);
    
    virtual std::string const & getName() const;

    /*
    ** CANONICAL FUNCS
    */
    Character();
    Character(Character & src);
  	Character & operator=(Character const & rhs);
    virtual ~Character();

  private:
    AMateria *_mat[4];
    std::string _name;

    bool doesMateriaIdxExist(int idx) const;
};

#endif

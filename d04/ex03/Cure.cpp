#include <iostream>
#include <string>
#include "Cure.hpp"
#include "ICharacter.hpp"
#include "AMateria.hpp"

void Cure::use(ICharacter& target){
	AMateria::use(target);
	std::cout << "* heals " << target.getName() << "’s wounds *" << std::endl;
}

Cure* Cure::clone() const{
	Cure *ret = new Cure();
	ret->setXP(this->getXP());
	return (ret);
}

/*
** CANONICAL FUNCS
*/

Cure::Cure(void) : AMateria("cure"){
	return;
}

Cure::Cure(Cure const & src) : AMateria("cure"){
	*this = src;
	return;
}

Cure &	Cure::operator=(Cure const & rhs){
  setXP(rhs.getXP());
	return *this;
}

Cure::~Cure(void){
	return;
}

#include "MateriaSource.hpp"
#include "AMateria.hpp"
#include <iostream>
#include <string>

AMateria* MateriaSource::createMateria(std::string const & type){
  for (size_t i = 0; i < 4; i++)
    if (this->_materias[i]->getType() == type)
      return (this->_materias[i]->clone());
  return (0);
}

void MateriaSource::learnMateria(AMateria* m){
  for (size_t i = 0; i < 4; i++) {
    if (this->_materias[i] == NULL)
    {
      this->_materias[i] = m->clone();
      return ;
    }
  }
}

/*
** CANONICAL FUNCS
*/

MateriaSource::MateriaSource(void){
  for (size_t i = 0; i < 4; i++)
    _materias[i] = NULL;
	return;
}

MateriaSource::MateriaSource(MateriaSource const & src){
	*this = src;
	return;
}

MateriaSource &	MateriaSource::operator=(MateriaSource const & rhs){
  for (size_t i = 0; i < 4; i++)
  {
    if (_materias[i] != NULL)
      delete(_materias[i]);
    _materias[i] = rhs._materias[i];
  }
	return *this;
}

MateriaSource::~MateriaSource(void){
  for (size_t i = 0; i < 4; i++)
    if (_materias[i] != NULL)
      delete(_materias[i]);
	return;
}

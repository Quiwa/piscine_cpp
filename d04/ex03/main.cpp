#include "AMateria.hpp"
#include "Character.hpp"
#include "IMateriaSource.hpp"
#include "MateriaSource.hpp"
#include "Ice.hpp"
#include "Cure.hpp"
#include <iostream>

int main(void)
{
  IMateriaSource* src = new MateriaSource();
  src->learnMateria(new Ice());
  src->learnMateria(new Cure());

  AMateria* tmp;
  tmp = src->createMateria("ice");

  ICharacter* me = new Character("me");
  me->equip(tmp);

  tmp = src->createMateria("cure");
  me->equip(tmp);

  ICharacter* bob = new Character("bob");
  me->use(0, *bob);
  me->use(1, *bob);
  me->use(2, *bob);
  delete bob;
  me->use(1, *bob);
  delete me;
  delete src;
  return 0;
}

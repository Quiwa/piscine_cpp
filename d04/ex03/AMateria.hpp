#ifndef AMATERIA_H
# define AMATERIA_H

#include <iostream>

class ICharacter;

class AMateria
{
  public:
    virtual void use(ICharacter& target);
    virtual AMateria* clone() const = 0;

    /*
    ** ACCESSORS
    */
    std::string const & getType() const; //Returns the materia type
    unsigned int getXP() const; //Returns the Materia's XP
    void setXP(unsigned int);

    /*
    ** CANONICAL FUNCS
    */
    AMateria(std::string const & type);
	  AMateria & operator=(AMateria const & rhs);
    AMateria(void);
    virtual ~AMateria();
    
  private:
    const std::string _type;
    unsigned int _xp;
};

#endif

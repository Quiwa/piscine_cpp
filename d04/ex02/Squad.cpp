#include "Squad.hpp"
#include "ISpaceMarine.hpp"
#include <iostream>

 int Squad::getCount() const{
    List * trav = _units;
    int i = 0;
    while (trav != NULL && ++i)
      trav = trav->next;
    return (i);
 }

 ISpaceMarine* Squad::getUnit(int idx) const{
    List * trav = _units;
    int i = -1;
    while (trav != NULL){
      if (++i == idx)
        return (trav->spaceMarine);
      trav = trav->next;
    }
  return (NULL);
 }

 int Squad::push(ISpaceMarine* unit){
    List * trav = _units;

    if (unit == NULL || isUnitRegistered(unit))
      return (getCount());
    List * toAdd = new List(unit);
    if (_units == NULL){
      _units = toAdd;
      return (1);
    }
    while (trav->next != NULL)
      trav = trav->next;
    trav->next = toAdd;
    return (getCount());
 }

bool Squad::isUnitRegistered(ISpaceMarine *spaceMarine) const{
  List * trav = _units;
  while (trav != NULL){
    if (trav->spaceMarine == spaceMarine)
      return (true);
    trav = trav->next;
  }
  return (false);
}

/*
** CANONICAL FUNCS
*/
Squad::Squad(void){
  _units = NULL;
}


Squad::Squad(Squad const & src){
  this->_units = NULL;
  *this = src;
}

Squad::~Squad(void){
  std::cout << "destructing Squad" << std::endl;
  if (_units != NULL)
    delete(_units);
}

Squad & Squad::operator=(Squad const & rhs){
  List * rhsTrav = rhs._units;

  std::cout << "WOW" << std::endl;
  std::cout << "assignation operator" << std::endl;
  if (_units != NULL)
    delete(_units);
  _units = NULL;
  while (rhsTrav != NULL){
    push(rhsTrav->spaceMarine->clone());
    rhsTrav = rhsTrav->next;
  }
  return (*this);
}

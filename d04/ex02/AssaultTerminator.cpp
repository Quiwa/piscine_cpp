#include "AssaultTerminator.hpp"
#include "ISpaceMarine.hpp"
#include <iostream>

AssaultTerminator* AssaultTerminator::clone() const{
  AssaultTerminator *cpy = new AssaultTerminator();
  *cpy = *this;
  return (cpy);
}

void AssaultTerminator::battleCry() const{
  std::cout << "This code is unclean. PURIFY IT!" << std::endl;
}

void AssaultTerminator::rangedAttack() const{
  std::cout << "* does nothing *" << std::endl;
}

void AssaultTerminator::meleeAttack() const{
  std::cout << "* attacks with chainfists *" << std::endl;
}

/*
** CANONICAL FUNCS
*/
AssaultTerminator::AssaultTerminator(void){
  std::cout << "* teleports from space *" << std::endl;
}

AssaultTerminator::AssaultTerminator(AssaultTerminator const & src){
  std::cout << "* teleports from space *" << std::endl;
  *this = src;
}

AssaultTerminator::~AssaultTerminator(void){
  std::cout << "I'll be back..." << std::endl;
}

AssaultTerminator & AssaultTerminator::operator=(AssaultTerminator const & ){
  return (*this);
}

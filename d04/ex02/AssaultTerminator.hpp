#ifndef ASSAULTTERMINATOR_H
# define ASSAULTTERMINATOR_H

#include "ISpaceMarine.hpp"

class AssaultTerminator : public ISpaceMarine{
  public:
    virtual AssaultTerminator* clone() const;
    virtual void battleCry() const;
    virtual void rangedAttack() const;
    virtual void meleeAttack() const;

    /*
    ** CANONICAL FUNCS
    */
    AssaultTerminator(void);
    AssaultTerminator(AssaultTerminator const & src);
    virtual ~AssaultTerminator(void);
  	AssaultTerminator & operator=(AssaultTerminator const & rhs);
};

#endif

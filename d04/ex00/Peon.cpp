#include "Peon.hpp"
#include <iostream>
#include <string>

void Peon::getPolymorphed(void) const{
  std::cout << this->_name << " has been turned into a pink pony!" << std::endl;
}

Peon::Peon(std::string name)
:
Victim(name)
{
  std::cout << "Zog zog." << std::endl;
	return;
}

Peon::Peon(void){
  std::cout << "Zog zog." << std::endl;
	return;
}


Peon::Peon(Peon const & src){
	*this = src;
  std::cout << "Zog zog." << std::endl;
	return;
}

Peon &	Peon::operator=(Peon const & rhs)
{
  Victim::operator=(rhs);
	return *this;
}

Peon::~Peon(void){
  std::cout << "Bleuark..." << std::endl;
	return;
}

#ifndef CAT_H
# define CAT_H

#include <iostream>
#include <string>
#include "Victim.hpp"

class Cat : public Victim{

public:
	Cat(void);
	Cat(std::string);
	Cat(Cat const & src);
	virtual ~Cat(void);
	Cat & operator=(Cat const & rhs);
	virtual void getPolymorphed(void) const;
};

#endif

#include "Dog.hpp"
#include <iostream>
#include <string>

void Dog::getPolymorphed(void) const{
  std::cout << this->_name << " has been turned into a pink pony!" << std::endl;
}

Dog::Dog(std::string name)
:
Victim(name)
{
  std::cout << "Waf waf." << std::endl;
	return;
}

Dog::Dog(void){
  std::cout << "Waf waf." << std::endl;
	return;
}


Dog::Dog(Dog const & src){
  std::cout << "Waf waf." << std::endl;
	*this = src;
	return;
}

Dog &	Dog::operator=(Dog const & rhs)
{
  Victim::operator=(rhs);
	return *this;
}

Dog::~Dog(void){
  std::cout << "Bleuark... waf" << std::endl;
	return;
}

#include "Sorcerer.hpp"
#include "Victim.hpp"
#include <iostream>
#include <string>

void Sorcerer::polymorph(Victim const &victim) const{
  victim.getPolymorphed();
}

Sorcerer::Sorcerer(std::string name, std::string title)
:
_name(name), _title(title)
{
  std::cout << this->_name << ", " << this->_title << ", is born !" << std::endl;
}

/*
** CANONICAL FUNCS
*/
Sorcerer::Sorcerer(Sorcerer const & src){
  *this = src;
  std::cout << this->_name << ", " << this->_title << ", is born !" << std::endl;
	return;
}

Sorcerer &	Sorcerer::operator=(Sorcerer const & rhs){
  this->_name = rhs.getName();
  this->_title = rhs.getTitle();
	return *this;
}

Sorcerer::~Sorcerer(void){
	std::cout << this->_name << ", " << this->_title << ", is dead. Consequences will never be the same!" << std::endl;
  return;
}

/*
** ACCESSORS
*/
const std::string & Sorcerer::getName(void) const{
  return (this->_name);
}

const std::string & Sorcerer::getTitle(void) const{
  return (this->_title);
}

std::ostream & operator<<(std::ostream &os, Sorcerer & sorcerer){
  os << "I am " << sorcerer.getName() << ", " << sorcerer.getTitle() << ", and I like ponies!" << std::endl;
  return (os);
}

#ifndef SORCERER_H
# define SORCERER_H

#include <iostream>
#include <string>
#include "Victim.hpp"

class Sorcerer{

public:

	void polymorph(Victim const &victim) const;

	/*
	** CANONICAL FUNCS
	*/
	Sorcerer(std::string name, std::string title);
	Sorcerer(Sorcerer const & src);
	virtual ~Sorcerer(void);
	Sorcerer & operator=(Sorcerer const & rhs);

	/*
	** ACCESSORS
	*/
	const std::string & getName(void) const;
	const std::string & getTitle(void) const;

private:
	std::string _name;
	std::string _title;

	Sorcerer(void);

};

std::ostream & operator<<(std::ostream &os, Sorcerer & sorcerer);

#endif

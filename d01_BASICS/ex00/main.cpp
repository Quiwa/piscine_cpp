#include "Pony.hpp"
#include <iostream>
#include <string>

void ponyOnTheHeap(std::string name){
  Pony *pony = new Pony(name);
  pony->run();
  delete pony;
}

void ponyOnTheStack(std::string name){
  Pony pony (name);
  pony.run();
}

int main(void)
{
  ponyOnTheHeap("heap");
  ponyOnTheStack("stack");
  return (1);
}

#include "Brain.hpp"
#include <string>
#include <iostream>
#include <sstream>

std::string Brain::identify(void) const {
  std::stringstream str;
  str << this;
  return (str.str());
}

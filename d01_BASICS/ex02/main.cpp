#include "Zombie.hpp"
#include "ZombieEvent.hpp"

int main(void)
{
  Zombie *zombie;
  ZombieEvent zombieEvent;

  zombie = zombieEvent.newZombie("Zombie-01");
  zombie->announce();
  delete(zombie);

  zombieEvent.setZombieType("walker");

  zombie = zombieEvent.newZombie("Zombie-02");
  zombie->announce();
  delete(zombie);

  zombieEvent.randomChump();
}

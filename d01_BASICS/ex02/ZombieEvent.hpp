#ifndef ZOMBIE_EVENT_H
# define ZOMBIE_EVENT_H

#include <string>
#include "Zombie.hpp"

class ZombieEvent{

public:
  void setZombieType(const std::string &);
  const std::string & getType(void) const;
  Zombie * newZombie (std::string name) const;
  void randomChump(void) const;

private:
  std::string type;
};


#endif

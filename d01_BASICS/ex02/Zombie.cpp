#include "Zombie.hpp"
#include <string>
#include <iostream>

void Zombie::announce(void) const {
  std::cout << "<" << this->getName() << " (" << this-> getType() << ")> Braiiiiiiinnnssss ..." << std::endl;
}

void Zombie::setName(const std::string & name){
  this->name = name;
}

void Zombie::setType(const std::string & type){
  this->type = type;
}

const std::string & Zombie::getName() const {
  return (this->name);
}

const std::string & Zombie::getType() const{
  return (this->type);
}

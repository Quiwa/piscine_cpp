#include "Zombie.hpp"
#include "ZombieEvent.hpp"
#include <string>
#include <iostream>

void ZombieEvent::setZombieType(const std::string & name){
  this->type = name;
}

const std::string & ZombieEvent::getType(void) const{
  return (this->type);
}

Zombie * ZombieEvent::newZombie(std::string name) const{
  Zombie *zombie = new Zombie();

  zombie->setType(this->getType());
  zombie->setName(name);
  return (zombie);
}

void ZombieEvent::randomChump(void) const{
  int nb = std::rand() % 5;
  std::string zombieName = "Zombie-";
  Zombie zombie;

  srand (time(NULL));
  for (int i = 0; i <= nb; i++) {
    zombieName += std::rand() % 10 + '0';
  }
  zombie.setName(zombieName);
  zombie.setType(this->getType());
  zombie.announce();
}

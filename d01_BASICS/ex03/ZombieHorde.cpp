#include "Zombie.hpp"
#include "ZombieHorde.hpp"

ZombieHorde::ZombieHorde(int n){
  if (n < 0)
  {
    this->n = 0;
    this->zombieHorde = new Zombie[0];
    return ;
  }
  this->n = n;
  this->zombieHorde = new Zombie[this->n];
  for (int i = 0; i < this->n; i++) {
    this->zombieHorde[i].setName(this->getRandomName(i));
  }
}

std::string ZombieHorde::getRandomName(int j) const{
  int nb = std::rand() % 4;
  std::string zombieName = "Zombie-";

  for (int i = 0; i <= nb; i++) {
    srand (time(NULL));
    zombieName += (std::rand() + j + i) % 10 + '0';
  }
  return (zombieName);
}

ZombieHorde::~ZombieHorde(void){
  delete [] this->zombieHorde;
}

void ZombieHorde::announce(void) const{
  for (int i = 0; i < this->n; i++)
    zombieHorde[i].announce();
}

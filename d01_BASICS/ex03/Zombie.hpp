#ifndef ZOMBIE_H
# define ZOMBIE_H

#include <string>

class Zombie{

public:
  void announce(void) const;

  void setType(const std::string &);
  void setName(const std::string &);

  const std::string & getType(void) const;
  const std::string & getName(void) const;

private:
  std::string type;
  std::string name;
};

#endif

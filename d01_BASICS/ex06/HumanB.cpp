#include "HumanB.hpp"
#include "Weapon.hpp"
#include <string>
#include <iostream>

HumanB::HumanB(std::string name){
  _name = name;
  _weapon = NULL;
}

void HumanB::attack(void) const{
  if (_weapon != NULL)
    std::cout << this->_name << " attacks with its " << this->_weapon->getType() << std::endl;
}

void HumanB::setWeapon(Weapon &weapon){
  this->_weapon = &weapon;
}

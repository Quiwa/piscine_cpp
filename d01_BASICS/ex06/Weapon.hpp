#ifndef WEAPON_H
# define WEAPON_H

#include <string>
#include <iostream>

class Weapon{

public:
  Weapon(std::string type);

  void setType(std::string const & type);
  const std::string & getType(void) const;

private:
  std::string _type;
};

#endif

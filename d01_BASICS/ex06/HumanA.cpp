#include "HumanA.hpp"
#include "Weapon.hpp"
#include <string>
#include <iostream>

HumanA::HumanA(std::string name, Weapon &weapon) : _weapon(weapon){
  this->_name = name;
}

void HumanA::attack(void) const{
  std::cout << this->_name << " attacks with its " << this->_weapon.getType() << std::endl;
}

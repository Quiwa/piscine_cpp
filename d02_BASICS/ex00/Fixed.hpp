#ifndef FIXED_H
# define FIXED_H

#include <iostream>
#include <string>

class Fixed{

public:

	/*
	** CANONICAL FUNCS
	*/
	Fixed(void);
	Fixed(Fixed const & src);
	~Fixed(void);
	Fixed & operator=(Fixed const & rhs);

	/*
	** ACCESSORS
	*/
  int getRawBits(void) const;
  void setRawBits(int const nb);

private:
  int _fixedPoint;
  static const int _bits = 8;

};

#endif
